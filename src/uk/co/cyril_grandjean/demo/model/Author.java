package uk.co.cyril_grandjean.demo.model;

import uk.co.cyril_grandjean.ddbstoolkit.core.DistributedEntity;
import uk.co.cyril_grandjean.ddbstoolkit.core.Id;
import uk.co.cyril_grandjean.ddbstoolkit.module.datastore.sparql.*;

/**
 * Class representing an author
 * User: Cyril GRANDJEAN
 * Date: 26/06/2012
 * Time: 11:09
 *
 * @version Creation of the class
 */
@Service(url="http://www.factforge.net/sparql")
@DefaultNamespace(name="fb",url="http://rdf.freebase.com/ns/")
public class Author extends DistributedEntity {

    @Id
    @Optional
    public int author_id;

    @URI
    @Namespace(name = "dbp-ont", url = "http://dbpedia.org/ontology/")
    public String author_uri;

    @Namespace(name = "foaf", url = "http://xmlns.com/foaf/0.1/")
    public String name;

    @Optional
    public int book_id;
}


