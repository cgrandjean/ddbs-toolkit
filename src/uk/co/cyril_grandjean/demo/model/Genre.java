package uk.co.cyril_grandjean.demo.model;

import uk.co.cyril_grandjean.ddbstoolkit.core.DistributedEntity;
import uk.co.cyril_grandjean.ddbstoolkit.core.Id;
import uk.co.cyril_grandjean.ddbstoolkit.core.PropertyName;
import uk.co.cyril_grandjean.ddbstoolkit.module.datastore.sparql.DefaultNamespace;
import uk.co.cyril_grandjean.ddbstoolkit.module.datastore.sparql.Optional;
import uk.co.cyril_grandjean.ddbstoolkit.module.datastore.sparql.Service;
import uk.co.cyril_grandjean.ddbstoolkit.module.datastore.sparql.URI;

/**
 * Class representing a genre of book
 * User: Cyril GRANDJEAN
 * Date: 26/06/2012
 * Time: 11:10
 *
 * @version Creation of the class
 */
@Service(url="http://www.factforge.net/sparql")
@DefaultNamespace(name="fb",url="http://rdf.freebase.com/ns/")
public class Genre extends DistributedEntity {

    @Id
    @Optional
    public int genre_id;

    @URI
    public String genre_uri;

    @PropertyName(name = "type.object.name")
    public String name;
}


