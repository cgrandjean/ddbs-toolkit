package uk.co.cyril_grandjean.demo.model;

import uk.co.cyril_grandjean.ddbstoolkit.core.DistributedEntity;
import uk.co.cyril_grandjean.ddbstoolkit.core.Id;
import uk.co.cyril_grandjean.ddbstoolkit.core.PropertyName;
import uk.co.cyril_grandjean.ddbstoolkit.module.datastore.sparql.*;

/**
 * Class representing a character
 * User: Cyril GRANDJEAN
 * Date: 26/06/2012
 * Time: 11:10
 *
 * @version Creation of the class
 */
@Service(url="http://www.factforge.net/sparql")
@DefaultNamespace(name="fb",url="http://rdf.freebase.com/ns/")
public class Character extends DistributedEntity {

    @Id
    @Optional
    public int character_id;

    @URI
    public String character_uri;

    @Namespace(name = "rdfs", url = "http://www.w3.org/2000/01/rdf-schema#")
    @PropertyName(name = "label")
    public String character_name;

    @Optional
    public int book_id;
}


