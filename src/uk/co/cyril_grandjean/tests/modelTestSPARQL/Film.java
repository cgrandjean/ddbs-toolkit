package uk.co.cyril_grandjean.tests.modelTestSPARQL;

import uk.co.cyril_grandjean.ddbstoolkit.core.IEntity;
import uk.co.cyril_grandjean.ddbstoolkit.core.Id;
import uk.co.cyril_grandjean.ddbstoolkit.module.datastore.sparql.DefaultNamespace;
import uk.co.cyril_grandjean.ddbstoolkit.module.datastore.sparql.Namespace;
import uk.co.cyril_grandjean.ddbstoolkit.module.datastore.sparql.Service;
import uk.co.cyril_grandjean.ddbstoolkit.module.datastore.sparql.URI;

/**
 * Entity representing a film
 * User: Cyril GRANDJEAN
 * Date: 19/06/2012
 * Time: 14:04
 *
 * @version Creation of the class
 */
@Service(url="http://data.linkedmdb.org/sparql")
@DefaultNamespace(name="movie",url="http://data.linkedmdb.org/resource/movie/")
public class Film implements IEntity {

    @Id
    public int filmid;

    @URI
    public String film_uri;

    @Namespace(name="dc",url="http://purl.org/dc/terms/")
    public String title;

    public int runtime;

    public Actor[] actor;
}
