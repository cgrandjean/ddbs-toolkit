package uk.co.cyril_grandjean.tests.modelTestSPARQL;

import uk.co.cyril_grandjean.ddbstoolkit.core.DistributedEntity;
import uk.co.cyril_grandjean.ddbstoolkit.core.Id;
import uk.co.cyril_grandjean.ddbstoolkit.module.datastore.sparql.DefaultNamespace;
import uk.co.cyril_grandjean.ddbstoolkit.module.datastore.sparql.Optional;
import uk.co.cyril_grandjean.ddbstoolkit.module.datastore.sparql.URI;

/**
 * Entity representing a company
 * User: Cyril GRANDJEAN
 * Date: 19/06/2012
 * Time: 15:56
 *
 * @version Creation of the class
 */
@DefaultNamespace(name="business",url="http://cyril-grandjean.co.uk/business/")
public class Company extends DistributedEntity {

    @URI
    public String company_uri;

    @Id
    public int company_ID;

    public String company_name;

    @Optional
    public String[] boss;

    public int[] mark;

    public long[] number1;

    public float[] number2;

    public int surface;

    public long longField;

    public float floatField;

    public Employee[] employee;
}
