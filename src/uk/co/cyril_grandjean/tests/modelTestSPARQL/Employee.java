package uk.co.cyril_grandjean.tests.modelTestSPARQL;

import uk.co.cyril_grandjean.ddbstoolkit.core.DistributedEntity;
import uk.co.cyril_grandjean.ddbstoolkit.core.Id;
import uk.co.cyril_grandjean.ddbstoolkit.module.datastore.sparql.DefaultNamespace;
import uk.co.cyril_grandjean.ddbstoolkit.module.datastore.sparql.Namespace;
import uk.co.cyril_grandjean.ddbstoolkit.module.datastore.sparql.URI;

/**
 * Entity representing an employee
 * User: Cyril GRANDJEAN
 * Date: 19/06/2012
 * Time: 15:56
 *
 * @version Creation of the class
 */
@DefaultNamespace(name="business",url="http://cyril-grandjean.co.uk/business/")
public class Employee extends DistributedEntity {

    @URI
    public String employee_uri;

    @Id
    private int employee_id;

    @Namespace(name="foaf",url="http://xmlns.com/foaf/0.1/")
    public String name;
}
