package uk.co.cyril_grandjean.tests.modelTestSPARQL;

import uk.co.cyril_grandjean.ddbstoolkit.core.DistributedEntity;
import uk.co.cyril_grandjean.ddbstoolkit.core.Id;
import uk.co.cyril_grandjean.ddbstoolkit.module.datastore.sparql.DefaultNamespace;
import uk.co.cyril_grandjean.ddbstoolkit.module.datastore.sparql.Optional;
import uk.co.cyril_grandjean.ddbstoolkit.module.datastore.sparql.Service;
import uk.co.cyril_grandjean.ddbstoolkit.module.datastore.sparql.URI;

/**
 * Entity representing an actor
 * User: Cyril GRANDJEAN
 * Date: 19/06/2012
 * Time: 14:02
 *
 * @version Creation of the class
 */
@Service(url="http://www.factforge.net/sparql")
@DefaultNamespace(name="fb",url="http://rdf.freebase.com/ns/")
public class Actor extends DistributedEntity {

    @Id
    @Optional
    public int actor_id;

    @URI
    public String actor_uri;

    public String actor_name;
}

