package uk.co.cyril_grandjean.tests.modelTestDatabase;

import uk.co.cyril_grandjean.ddbstoolkit.core.DistributedEntity;
import uk.co.cyril_grandjean.ddbstoolkit.core.Id;

/**
 * Model linked to table "tag"
 * @author ydamato
 * @version 1.0 Class creation
 */
public class Tag extends DistributedEntity {
	
	private static final long serialVersionUID = 1L;

	/**
	 * Tag id
	 */
	@Id
	public int tag_id;
	
	/**
	 * Tagmap id
	 */
	public int tagmap_id;
	
	/**
	 * Tag name
	 */
	public String name;
	
	/**
	 * Tag value
	 */
	public String value;
	
	/**
	 * Tag explanation
	 */
	public String explanation;
	
	/**
	 * Default constructor
	 */
	public Tag() {
		
	}
	
	/**
	 * Constructor
	 * @param tagmap_id
	 * 		Tagmap id
	 * @param name
	 * 		Tagmap name
	 * @param value
	 * 		Tagmap value
	 * @param explanation
	 * 		Tagmap explanation
	 */
	public Tag(int tagmap_id, String name, String value, String explanation) {
		this.tagmap_id = tagmap_id;
		this.name = name;
		this.value = value;
		this.explanation = explanation;
	}
	
	public int getTagmap_id() {
		return tagmap_id;
	}

	public void setTagmap_id(int tagmap_id) {
		this.tagmap_id = tagmap_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getExplanation() {
		return explanation;
	}

	public void setExplanation(String explanation) {
		this.explanation = explanation;
	}
}
