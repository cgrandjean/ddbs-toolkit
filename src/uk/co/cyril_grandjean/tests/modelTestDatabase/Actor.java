package uk.co.cyril_grandjean.tests.modelTestDatabase;

import uk.co.cyril_grandjean.ddbstoolkit.core.DistributedEntity;
import uk.co.cyril_grandjean.ddbstoolkit.core.IEntity;
import uk.co.cyril_grandjean.ddbstoolkit.core.Id;
import uk.co.cyril_grandjean.ddbstoolkit.core.ImplementableEntity;
import uk.co.cyril_grandjean.ddbstoolkit.core.exception.DDBSToolkitException;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Entity representing an actor
 * User: Cyril GRANDJEAN
 * Date: 18/06/2012
 * Time: 15:24
 *
 * @version Creation of the class
 */
public class Actor extends DistributedEntity implements ImplementableEntity {

    @Id
    public int actor_id;

    public String actor_name;

    public int film_ID;

    @Override
    public <T extends IEntity> ArrayList<T> conversionResultSet(ResultSet results, T myObject) throws DDBSToolkitException {

        ArrayList<T> resultList = new ArrayList<T>();

        try {
			while(results.next()){

			    Actor entity = new Actor();
			    entity.actor_id = results.getInt("actor_id");
			    entity.actor_name = results.getString("actor_name");
			    entity.film_ID = results.getInt("film_ID");

			    resultList.add((T)entity);
			}
		} catch (SQLException sqle) {
			throw new DDBSToolkitException("Error during execution of the SQL request", sqle);
		}
        return resultList;
    }
}
