package uk.co.cyril_grandjean.tests.modelTestDatabase;

import uk.co.cyril_grandjean.ddbstoolkit.core.DistributedEntity;
import uk.co.cyril_grandjean.ddbstoolkit.core.IEntity;
import uk.co.cyril_grandjean.ddbstoolkit.core.Id;
import uk.co.cyril_grandjean.ddbstoolkit.core.ImplementableEntity;
import uk.co.cyril_grandjean.ddbstoolkit.core.exception.DDBSToolkitException;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;

/**
 * Entity representing a film
 * User: Cyril GRANDJEAN
 * Date: 18/06/2012
 * Time: 15:21
 *
 * @version Creation of the class
 */
public class Film extends DistributedEntity implements ImplementableEntity {

    @Id
    public int film_ID;

    public String film_name;

    public int duration;

    public Timestamp creationDate;

    public long longField;

    public float floatField;

    public Actor[] actors;

    @Override
    public <T extends IEntity> ArrayList<T> conversionResultSet(ResultSet results, T myObject) throws DDBSToolkitException {

    	try
    	{
    		ArrayList<T> resultList = new ArrayList<T>();

            while(results.next()){

                Film entity = new Film();
                entity.film_ID = results.getInt("film_ID");
                entity.film_name = results.getString("film_name");
                entity.duration = results.getInt("duration");
                entity.creationDate = results.getTimestamp("creationDate");
                entity.longField = results.getLong("longField");
                entity.floatField = results.getFloat("floatField");

                resultList.add((T)entity);
                
                return resultList;
            }
    	}
    	catch (SQLException sqle) {
			throw new DDBSToolkitException("Error during execution of the SQL request", sqle);
		}
		return null;
    }
}
