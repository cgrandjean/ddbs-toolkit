package uk.co.cyril_grandjean.tests.main;

import uk.co.cyril_grandjean.ddbstoolkit.module.middleware.jgroups.JGroupReceiver;
import uk.co.cyril_grandjean.ddbstoolkit.module.datastore.sparql.DistributedSPARQLManager;

import java.util.Scanner;

/**
 * Listener server with JGroups + SPARQL
 * User: Cyril GRANDJEAN
 * Date: 25/06/2012
 * Time: 15:45
 *
 * @version Creation of the class
 */
public class JGroupSPARQLListenerProgram {

    /**
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {

        System.out.println("JGroups Receiver");

        System.out.println("Instantiate the manager");

        Scanner sc = new Scanner(System.in);

        DistributedSPARQLManager manager;
        if(args.length >= 1)
        {
            manager = new DistributedSPARQLManager(args[0]);
        }
        else {

            System.out.println("What is the path of the datastore ?");
            String path = sc.nextLine();


            manager = new DistributedSPARQLManager(path);
        }


        System.out.println("Instantiate the receiver");
        JGroupReceiver receiver;

        if(args.length >= 3)
        {
            receiver = new JGroupReceiver(manager, args[1], args[2]);
        }
        else
        {
            System.out.println("What is the name of the cluster ?");
            String clusterName = sc.nextLine();

            System.out.println("What is the name of the peer ?");
            String peerName = sc.nextLine();

            receiver = new JGroupReceiver(manager, clusterName, peerName);
        }


        try {
            System.out.println("Start the listener : Enter a touch to stop");
            receiver.start();

            System.out.println("Peer "+receiver.getMyPeer().getName()+" "+receiver.getMyPeer().getUid());

            sc.nextLine();

        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            receiver.stop();

            System.out.println("Stop the listener");
        }
    }
}
