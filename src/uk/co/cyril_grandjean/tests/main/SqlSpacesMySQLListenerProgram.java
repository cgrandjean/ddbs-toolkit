package uk.co.cyril_grandjean.tests.main;

import uk.co.cyril_grandjean.ddbstoolkit.module.datastore.mysql.DistributedMySQLTableManager;
import uk.co.cyril_grandjean.ddbstoolkit.module.datastore.mysql.MySQLConnector;
import uk.co.cyril_grandjean.ddbstoolkit.module.middleware.sqlspaces.SqlSpacesReceiver;

import java.util.Scanner;

/**
 * Listener server with SQLSpaces + MySQL
 * User: Cyril GRANDJEAN
 * Date: 21/06/2012
 * Time: 13:41
 *
 * @version Creation of the class
 */
public class SqlSpacesMySQLListenerProgram {

    /**
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {

        System.out.println("SQLSpaces Receiver");

        System.out.println("Instantiate the manager");

        Scanner sc = new Scanner(System.in);

        DistributedMySQLTableManager manager;
        if(args.length >= 3)
        {
            manager = new DistributedMySQLTableManager(new MySQLConnector(args[0], args[1], args[2]));
        }
        else {

            System.out.println("What is the URL to the database ?");
            String url = sc.nextLine();

            System.out.println("What is the login ?");
            String login = sc.nextLine();

            System.out.println("What is the password ?");
            String password = sc.nextLine();

            manager = new DistributedMySQLTableManager(new MySQLConnector(url, login, password));
        }


        System.out.println("Instantiate the receiver");
        SqlSpacesReceiver receiver;

        if(args.length >= 5)
        {
            receiver = new SqlSpacesReceiver(manager, args[3], args[4]);
        }
        else
        {
            System.out.println("What is the name of the cluster ?");
            String clusterName = sc.nextLine();

            System.out.println("What is the name of the peer ?");
            String peerName = sc.nextLine();

            receiver = new SqlSpacesReceiver(manager, clusterName, peerName);
        }


        try {
            System.out.println("Start the listener : Enter a touch to stop");
            receiver.start();

            System.out.println("Peer "+receiver.getMyPeer().getName()+" "+receiver.getMyPeer().getUid());

            sc.nextLine();

        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            receiver.stop();

            System.out.println("Stop the listener");
        }


    }
}
