package uk.co.cyril_grandjean.tests.junit;

import org.junit.Test;

import uk.co.cyril_grandjean.ddbstoolkit.module.datastore.mysql.DistributedMySQLTableManager;
import uk.co.cyril_grandjean.ddbstoolkit.module.datastore.mysql.MySQLConnector;
import uk.co.cyril_grandjean.tests.modelTestDatabase.Tag;

public class DDBSToolkitTest {

	@Test
    public void testAdd() throws Exception {

        DistributedMySQLTableManager manager = new DistributedMySQLTableManager(new MySQLConnector("jdbc:mysql://localhost:8889/aurora", "root", "root")) ;
        manager.open();
        
        Tag aTag = new Tag();
        
        manager.add(aTag);
        
        manager.close();
	}
}
