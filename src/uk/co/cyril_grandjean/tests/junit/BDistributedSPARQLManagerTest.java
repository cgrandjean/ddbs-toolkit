package uk.co.cyril_grandjean.tests.junit;

import org.junit.Test;
import uk.co.cyril_grandjean.ddbstoolkit.core.Peer;
import uk.co.cyril_grandjean.ddbstoolkit.module.datastore.sparql.DistributedSPARQLManager;
import uk.co.cyril_grandjean.demo.model.Book;
import uk.co.cyril_grandjean.tests.modelTestSPARQL.Company;
import uk.co.cyril_grandjean.tests.modelTestSPARQL.Employee;
import uk.co.cyril_grandjean.tests.modelTestSPARQL.Film;

import java.util.ArrayList;

/**
 * JUnit tests to test DistributedSPARQLManager class
 * User: Cyril GRANDJEAN
 * Date: 19/06/2012
 * Time: 13:54
 *
 * @version Creation of the class
 */
public class BDistributedSPARQLManagerTest {

    /**
     * Test the connector
     * @throws Exception
     */
    @Test
    public void testIsOpen() throws Exception {

        DistributedSPARQLManager manager = new DistributedSPARQLManager(new Peer(), "/Users/cyril/Desktop/datastore");

        manager.open();

        assert manager.isOpen() == true;

        manager.close();

        assert manager.isOpen() == false;
    }

    /**
     * JUnit tests for the listAll function for MySQL
     * @throws Exception
     */
    @Test
    public void testListAll() throws Exception {

        //Test with existing remote SPARQL endpoint
        DistributedSPARQLManager manager = new DistributedSPARQLManager();

        ArrayList<String> listCondition = new ArrayList<String>();
        listCondition.add(DistributedSPARQLManager.getObjectVariable(new Film())+" dc:title 'The Return of the King'");

        ArrayList<Film> listEntity = manager.listAll(new Film(), listCondition, null);

        //There is only one element
        assert listEntity.size() == 1;

        Film myFilm = (Film)listEntity.get(0);

        assert  myFilm.filmid == 1025;
        assert  myFilm.film_uri.equals("http://data.linkedmdb.org/resource/film/1025");
        assert  myFilm.title.equals(myFilm.title);
        assert  myFilm.runtime == 98;

        listCondition = new ArrayList<String>();
        listCondition.add(DistributedSPARQLManager.getObjectVariable(new Book())+" fb:type.object.name 'The Fellowship of the Ring'@en");
        listCondition.add("FILTER ( lang(?title) =  'en' )");
        listCondition.add("FILTER ( lang(?summary) = 'en' )");

        ArrayList<Book> listBook = manager.listAll(new Book(), listCondition, null);

        Book myBook = (Book)listBook.get(0);
        myBook = (Book) manager.loadArray(myBook, "author", "name ASC");
        myBook = (Book) manager.loadArray(myBook, "genre", null);
        myBook = (Book) manager.loadArray(myBook, "character", null);

        int test = 1;
    }

    /**
     * JUnit test to test the Read function
     * @throws Exception
     */
    @Test
    public void testRead() throws Exception {

        //Test with a remote SPARQL Endpoint
        DistributedSPARQLManager manager = new DistributedSPARQLManager();

        Film filmToRead = new Film();
        filmToRead.film_uri = "http://data.linkedmdb.org/resource/film/1025";

        Film filmExtracted = (Film) manager.read(filmToRead);

        assert  filmExtracted.filmid == 1025;
        assert  filmExtracted.film_uri.equals("http://data.linkedmdb.org/resource/film/1025");
        assert  filmExtracted.title.equals("The Return of the King");
        assert  filmExtracted.runtime == 98;
    }

    /**
     * JUnit tests to test that the last element correspond to the last element added
     * @throws Exception
     */
    @Test
    public void testReadLastElement() throws Exception {

        //Test with a remote SPARQL Endpoint
        DistributedSPARQLManager manager = new DistributedSPARQLManager();

        Film myFilm = (Film) manager.readLastElement(new Film());

        int test;
    }

    /**
     * JUnit tests for adding
     * @throws Exception
     */
    @Test
    public void testAdd() throws Exception {

        DistributedSPARQLManager manager = new DistributedSPARQLManager(new Peer(), "/Users/cyril/Desktop/datastore");
        manager.open();

        int size =  manager.listAll(new Company(), null, null).size();

        //Bad parameters
        assert manager.add(null) == false;

        //Bad parameters
        Company companyToTest = new Company();
        assert manager.add(null) == false;

        companyToTest.company_uri = "http://cyril-grandjean.co.uk/business/CompanyTest";
        manager.add(companyToTest);
        size++;

        ArrayList<Company> list = manager.listAll(new Company(), null, null);

        assert list.size() == size;

        manager.delete(companyToTest);
        size--;

        //Add a company
        Company companyToAdd = new Company();
        companyToAdd.company_uri = "http://cyril-grandjean.co.uk/business/Microsoft";
        companyToAdd.company_ID = 2;
        companyToAdd.company_name = "Microsoft";
        companyToAdd.floatField = 20;
        companyToAdd.longField = 30;
        companyToAdd.surface = 30;

        //Set 2 boss
        String[] boss = new String[2];
        boss[0] = "Bill Gates";
        boss[1] = "Steve Ballmer";
        companyToAdd.boss = boss;

        //Set 2 ints
        int[] mark = new int[2];
        mark[0] = 1;
        mark[1] = 2;
        companyToAdd.mark = mark;

        //Set 2 longs
        long[] number1 = new long[2];
        number1[0] = 3;
        number1[1] = 4;
        companyToAdd.number1 = number1;

        //Set 2 floats
        float[] number2 = new float[2];
        number2[0] = 5;
        number2[1] = 6;
        companyToAdd.number2 = number2;

        manager.add(companyToAdd);
        size++;

        list = manager.listAll(new Company(), null, null);

        assert list.size() == size;

        Company myCompany = (Company) manager.readLastElement(new Company());

        assert companyToAdd.company_uri.equals(myCompany.company_uri);
        assert companyToAdd.company_ID == myCompany.company_ID;
        assert companyToAdd.company_name.equals(myCompany.company_name);
        assert companyToAdd.floatField == myCompany.floatField;
        assert companyToAdd.longField == myCompany.longField;
        assert companyToAdd.surface == myCompany.surface;

        assert companyToAdd.boss[0].equals(myCompany.boss[0]);
        assert companyToAdd.boss[1].equals(myCompany.boss[1]);

        assert companyToAdd.mark[0] == myCompany.mark[0];
        assert companyToAdd.mark[1] == myCompany.mark[1];

        assert companyToAdd.number1[0] == myCompany.number1[0];
        assert companyToAdd.number1[1] == myCompany.number1[1];

        assert companyToAdd.number2[0] == myCompany.number2[0];
        assert companyToAdd.number2[1] == myCompany.number2[1];

        //Delete the company
        manager.delete(companyToAdd);

        manager.close();
    }

    /**
     * JUnit tests to test the update function
     * @throws Exception
     */
    @Test
    public void testUpdate() throws Exception {

        DistributedSPARQLManager manager = new DistributedSPARQLManager(new Peer(), "/Users/cyril/Desktop/datastore");
        manager.open();

        //Add a company
        Company companyToAdd = new Company();
        companyToAdd.company_uri = "http://cyril-grandjean.co.uk/business/Microsoft";
        companyToAdd.company_ID = 2;
        companyToAdd.company_name = "Microsoft";
        companyToAdd.floatField = 20;
        companyToAdd.longField = 30;
        companyToAdd.surface = 30;

        //Set 2 boss
        String[] boss = new String[2];
        boss[0] = "Bill Gates";
        boss[1] = "Steve Ballmer";
        companyToAdd.boss = boss;

        //Set 2 ints
        int[] mark = new int[2];
        mark[0] = 1;
        mark[1] = 2;
        companyToAdd.mark = mark;

        //Set 2 longs
        long[] number1 = new long[2];
        number1[0] = 3;
        number1[1] = 4;
        companyToAdd.number1 = number1;

        //Set 2 floats
        float[] number2 = new float[2];
        number2[0] = 5;
        number2[1] = 6;
        companyToAdd.number2 = number2;

        manager.add(companyToAdd);

        Company companyToUpdate = new Company();
        companyToUpdate.company_uri = "http://cyril-grandjean.co.uk/business/Microsoft";
        companyToUpdate.company_ID = 3;
        companyToUpdate.company_name = "Microsoft Corporation";
        companyToUpdate.floatField = 30;
        companyToUpdate.longField = 40;
        companyToUpdate.surface = 50;

        //Set 2 boss
        boss = new String[2];
        boss[0] = "Bill Gates Updated";
        boss[1] = "Steve Ballmer Updated";
        companyToUpdate.boss = boss;

        //Set 2 ints
        mark = new int[2];
        mark[0] = 3;
        mark[1] = 4;
        companyToUpdate.mark = mark;

        //Set 2 longs
        number1 = new long[2];
        number1[0] = 5;
        number1[1] = 6;
        companyToUpdate.number1 = number1;

        //Set 2 floats
        number2 = new float[2];
        number2[0] = 5;
        number2[1] = 6;
        companyToUpdate.number2 = number2;

        manager.update(companyToUpdate);

        Company myCompany = (Company) manager.readLastElement(new Company());

        assert companyToUpdate.company_uri.equals(myCompany.company_uri);
        assert companyToUpdate.company_ID == myCompany.company_ID;
        assert companyToUpdate.company_name.equals(myCompany.company_name);
        assert companyToUpdate.floatField == myCompany.floatField;
        assert companyToUpdate.longField == myCompany.longField;
        assert companyToUpdate.surface == myCompany.surface;

        assert companyToUpdate.boss[0].equals(myCompany.boss[0]);
        assert companyToUpdate.boss[1].equals(myCompany.boss[1]);

        assert companyToUpdate.mark[0] == myCompany.mark[0];
        assert companyToUpdate.mark[1] == myCompany.mark[1];

        assert companyToUpdate.number1[0] == myCompany.number1[0];
        assert companyToUpdate.number1[1] == myCompany.number1[1];

        assert companyToUpdate.number2[0] == myCompany.number2[0];
        assert companyToUpdate.number2[1] == myCompany.number2[1];

        manager.close();
    }

    /**
     * JUnit tests to test the delete function
     * @throws Exception
     */
    @Test
    public void testDelete() throws Exception {

        DistributedSPARQLManager manager = new DistributedSPARQLManager(new Peer(), "/Users/cyril/Desktop/datastore");
        manager.open();

        //If there is no primary key, must return false
        assert manager.delete(null) == false;

        //If there is no primary key, must return false
        assert manager.delete(new Company()) == false;

        ArrayList<Company> listEntities = manager.listAll(new Company(), null, null);

        //Delete all entities
        for(Company companyToDelete : listEntities)
        {
            manager.delete(companyToDelete);
        }

        //Check if all entities has been deleted
        assert manager.listAll(new Company(), null, null).size() == 0;

    }

    /**
     * JUnit tests to test the LoadArray function
     * @throws Exception
     */
    @Test
    public void testLoadArray() throws Exception {

        DistributedSPARQLManager manager = new DistributedSPARQLManager(new Peer(), "/Users/cyril/Desktop/datastore");
        manager.open();

        //Add a company
        Company companyToAdd = new Company();
        companyToAdd.company_uri = "http://cyril-grandjean.co.uk/business/Microsoft";
        companyToAdd.company_ID = 2;
        companyToAdd.company_name = "Microsoft";
        companyToAdd.floatField = 20;
        companyToAdd.longField = 30;
        companyToAdd.surface = 30;

        //Set 2 boss
        String[] boss = new String[2];
        boss[0] = "Bill Gates";
        boss[1] = "Steve Ballmer";
        companyToAdd.boss = boss;

        //Set 2 employees
        Employee employee1 = new Employee();
        employee1.employee_uri = "http://cyril-grandjean.co.uk/business/Cyril_Grandjean";
        employee1.name = "Cyril Grandjean";
        manager.add(employee1);

        Employee employee2 = new Employee();
        employee2.employee_uri = "http://cyril-grandjean.co.uk/business/Steve_Jobs";
        employee2.name = "Steve Jobs";
        manager.add(employee2);

        ArrayList<Employee> listEmployees = manager.listAll(new Employee(), null, null);
        assert listEmployees.size() == 2;

        Employee[] listEmployee = new Employee[2];
        companyToAdd.employee = listEmployee;
        companyToAdd.employee[0] = employee1;
        companyToAdd.employee[1] = employee2;

        //Add the company
        manager.add(companyToAdd);

        Company companyToLoad = (Company)manager.loadArray(companyToAdd, "employee", null);

        assert companyToLoad.employee.length == 2;

        for (int i = 0; i < companyToLoad.employee.length; i++)
        {
            assert companyToLoad.employee[i].employee_uri.equals(employee1.employee_uri) || companyToLoad.employee[i].employee_uri.equals(employee2.employee_uri);
            assert companyToLoad.employee[i].name.equals(employee1.name) || companyToLoad.employee[i].name.equals(employee2.name);

            manager.delete(companyToLoad.employee[i]);
        }

        manager.delete(companyToAdd);

        Film filmToRead = new Film();
        filmToRead.film_uri = "http://data.linkedmdb.org/resource/film/1025";

        //Test with remote endpoint
        Film filmExtracted = (Film) manager.read(filmToRead);

        assert  filmExtracted.filmid == 1025;
        assert  filmExtracted.film_uri.equals("http://data.linkedmdb.org/resource/film/1025");
        assert  filmExtracted.title.equals("The Return of the King");
        assert  filmExtracted.runtime == 98;

        manager.loadArray(filmExtracted, "actor", null);

        for (int i = 0; i < filmExtracted.actor.length; i++)
        {
            System.out.println(filmExtracted.actor[i].actor_uri+" "+filmExtracted.actor[i].actor_name);
        }

        manager.close();
    }
}
