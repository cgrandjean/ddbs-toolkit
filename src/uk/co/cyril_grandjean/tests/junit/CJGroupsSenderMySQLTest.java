package uk.co.cyril_grandjean.tests.junit;

import org.junit.Test;
import uk.co.cyril_grandjean.ddbstoolkit.core.Peer;
import uk.co.cyril_grandjean.ddbstoolkit.module.middleware.jgroups.JGroupSender;
import uk.co.cyril_grandjean.tests.modelTestDatabase.Actor;
import uk.co.cyril_grandjean.tests.modelTestDatabase.Film;

import java.sql.Timestamp;
import java.util.ArrayList;

/**
 * JUnit tests to test the JGroupSender class with MySQL
 * User: Cyril GRANDJEAN
 * Date: 25/06/2012
 * Time: 13:46
 *
 * @version Creation of the class
 */
public class CJGroupsSenderMySQLTest {

    /**
     * Test the connector
     * @throws Exception
     */
    @Test
    public void testIsOpen() throws Exception {

        JGroupSender sender = new JGroupSender("defaultCluster", "sender");

        sender.open();

        assert sender.isOpen() == true;

        sender.close();

        assert sender.isOpen() == false;
    }

    /**
     * JUnit tests for the list of peers
     * Condition 2 receivers must be launched
     * @throws Exception
     */
    @Test
    public void testGetListPeers() throws Exception {

        JGroupSender sender = new JGroupSender("defaultCluster", "sender");

        sender.open();

        ArrayList<Peer> list = sender.getListPeers();

        assert list.size() == 2;

        sender.close();
    }

    /**
     * JUnit tests for the listAll function for MySQL
     * @throws Exception
     */
    @Test
    public void testListAll() throws Exception {

        JGroupSender sender = new JGroupSender("defaultCluster", "sender");

        sender.open();

        //All parameters : Must return null
        assert sender.listAll(null, null, null) == null;

        //Select a movie
        assert sender.listAll(new Film(), null, null) != null;

        int sizeList = sender.listAll(new Film(), null, null).size();

        ArrayList<Peer> list = sender.getListPeers();

        //Foreach peer, we add 2 elements
        for (Peer myPeer : list)
        {
            Film filmToAdd = new Film();
            filmToAdd.film_name = "Test JUnit JGroups";
            filmToAdd.duration = 10;
            filmToAdd.floatField = new Float(20);
            filmToAdd.longField = new Long(100);
            filmToAdd.creationDate = new Timestamp(100000);
            filmToAdd.node_id = myPeer.getUid();

            sender.add(filmToAdd);

            testReadLastElement(filmToAdd);

            sizeList++;

            filmToAdd = new Film();
            filmToAdd.film_name = "Test JUnit JGroups";
            filmToAdd.duration = 20;
            filmToAdd.floatField = new Float(20);
            filmToAdd.longField = new Long(100);
            filmToAdd.creationDate = new Timestamp(100000);
            filmToAdd.node_id = myPeer.getUid();

            sender.add(filmToAdd);

            sizeList++;

            testReadLastElement(filmToAdd);
        }

        //Check that the size is 4
        ArrayList<Film> listFilms = sender.listAll(new Film(), null, null);

        assert listFilms.size() == sizeList;

        listFilms = sender.listAll(new Film(), null, "duration ASC");

        Film firstFilm = listFilms.get(0);
        int duration = firstFilm.duration;

        for(Film myFilm : listFilms)
        {
            assert myFilm.duration >= duration;
            duration = myFilm.duration;
        }

        listFilms = sender.listAll(new Film(), null, "duration DESC");

        firstFilm = listFilms.get(0);
        duration = firstFilm.duration;

        for(Film myFilm : listFilms)
        {
            assert myFilm.duration <= duration;
            duration = myFilm.duration;
        }

        sender.close();

    }

    /**
     * JUnit tests to test the Read function
     * @throws Exception
     */
    @Test
    public void testRead() throws Exception {

        JGroupSender sender = new JGroupSender("defaultCluster", "sender");

        sender.open();

        //No valid parameter, must be null
        assert sender.read(null) == null;

        //No valid parameter, must be null
        assert sender.read(new Film()) == null;

        ArrayList<Peer> list = sender.getListPeers();

        //Foreach peer, we add 2 elements
        for (Peer myPeer : list)
        {
            Film filmToAdd = new Film();
            filmToAdd.film_name = "Test JUnit SQL Spaces";
            filmToAdd.duration = 10;
            filmToAdd.floatField = new Float(20);
            filmToAdd.longField = new Long(100);
            filmToAdd.creationDate = new Timestamp(100000);
            filmToAdd.node_id = myPeer.getUid();

            sender.add(filmToAdd);

            testReadLastElement(filmToAdd);

            Film lastFilmAdded = (Film)sender.readLastElement(filmToAdd);
            Film myFilm = (Film)sender.read(lastFilmAdded);

            assert myFilm.node_id.equals(lastFilmAdded.node_id);
            assert myFilm.film_name.equals(lastFilmAdded.film_name);
            assert myFilm.duration == lastFilmAdded.duration;
            assert myFilm.floatField == lastFilmAdded.floatField;
            assert myFilm.longField == lastFilmAdded.longField;
            assert lastFilmAdded.creationDate.equals(lastFilmAdded.creationDate);
        }

        sender.close();
    }

    /**
     * Check the last element
     * @param objectToCheck Object to check
     * @throws Exception
     */
    public void testReadLastElement(Film objectToCheck) throws Exception {

        JGroupSender sender = new JGroupSender("defaultCluster", "sender");

        sender.open();

        //No valid parameter, must be null
        assert sender.readLastElement(null) == null;

        //No valid parameter, must be null
        assert sender.readLastElement(new Film()) == null;

        Film lastFilm = (Film)sender.readLastElement(objectToCheck);
        assert objectToCheck.node_id.equals(lastFilm.node_id);
        assert (objectToCheck.film_name == null || objectToCheck.film_name.equals(lastFilm.film_name));
        assert objectToCheck.duration == lastFilm.duration;
        assert objectToCheck.floatField == lastFilm.floatField;
        assert objectToCheck.longField == lastFilm.longField;
        assert objectToCheck.creationDate == null || objectToCheck.creationDate.equals(lastFilm.creationDate);

        sender.close();
    }

    /**
     * JUnit tests to test the Add function
     * @throws Exception
     */
    @Test
    public void testAdd() throws Exception {

        JGroupSender sender = new JGroupSender("defaultCluster", "sender");

        sender.open();

        //No valid parameter, must be null
        assert sender.add(null) == false;

        //No valid parameter, must be null
        assert sender.add(new Film()) == false;

        int numberOfElement = sender.listAll(new Film(), null, null).size();

        ArrayList<Peer> list = sender.getListPeers();

        //Foreach peer, we add 2 elements
        for (Peer myPeer : list)
        {
            //All parameters with no values
            Film filmToAdd = new Film();
            filmToAdd.node_id = myPeer.getUid();
            sender.add(filmToAdd);

            testReadLastElement(filmToAdd);

            numberOfElement++;

            //Add the string
            filmToAdd = new Film();
            filmToAdd.node_id = myPeer.getUid();
            filmToAdd.film_name = "Test JUnit 1";
            sender.add(filmToAdd);

            testReadLastElement(filmToAdd);

            numberOfElement++;

            //Add the duration
            filmToAdd = new Film();
            filmToAdd.node_id = myPeer.getUid();
            filmToAdd.film_name = "Test JUnit 2";
            filmToAdd.duration = 10;

            sender.add(filmToAdd);

            testReadLastElement(filmToAdd);

            numberOfElement++;

            //Add the float
            filmToAdd = new Film();
            filmToAdd.node_id = myPeer.getUid();
            filmToAdd.film_name = "Test JUnit 3";
            filmToAdd.duration = 10;
            filmToAdd.floatField = new Float(20);

            sender.add(filmToAdd);

            testReadLastElement(filmToAdd);

            numberOfElement++;

            //Add the long
            filmToAdd = new Film();
            filmToAdd.node_id = myPeer.getUid();
            filmToAdd.film_name = "Test JUnit 4";
            filmToAdd.duration = 10;
            filmToAdd.floatField = new Float(20);
            filmToAdd.longField = new Long(100);

            sender.add(filmToAdd);

            testReadLastElement(filmToAdd);

            numberOfElement++;

            //Add the timestamp
            filmToAdd = new Film();
            filmToAdd.node_id = myPeer.getUid();
            filmToAdd.film_name = "Test JUnit 5";
            filmToAdd.duration = 10;
            filmToAdd.floatField = new Float(20);
            filmToAdd.longField = new Long(100);
            filmToAdd.creationDate = new Timestamp(100000);

            sender.add(filmToAdd);

            testReadLastElement(filmToAdd);

            numberOfElement++;
        }

        System.out.println(sender.listAll(new Film(), null, null).size());

        assert  sender.listAll(new Film(), null, null).size() == numberOfElement;

        sender.close();
    }

    /**
     * JUnit tests to test the Update function
     * @throws Exception
     */
    @Test
    public void testUpdate() throws Exception {

        JGroupSender sender = new JGroupSender("defaultCluster", "sender");

        sender.open();

        //No valid parameter, must be null
        assert sender.update(null) == false;

        //No valid parameter, must be null
        assert sender.update(new Film()) == false;

        ArrayList<Peer> list = sender.getListPeers();

        //Foreach peer, we add 2 elements
        for (Peer myPeer : list)
        {
            //Add a movie
            Film filmToAdd = new Film();
            filmToAdd.node_id = myPeer.getUid();
            filmToAdd.film_name = "Test JUnit";
            filmToAdd.duration = 10;
            filmToAdd.floatField = new Float(20);
            filmToAdd.longField = new Long(30);
            filmToAdd.creationDate = new Timestamp(10000);

            sender.add(filmToAdd);

            //Check the last element
            testReadLastElement(filmToAdd);

            //Modify the last element
            Film lastFilmAdded = (Film)sender.readLastElement(filmToAdd);
            lastFilmAdded.film_name = "Test JUnit Updated";
            lastFilmAdded.duration = 20;
            lastFilmAdded.floatField = new Float(30);
            lastFilmAdded.longField = new Long(40);
            lastFilmAdded.creationDate = new Timestamp(20000);

            sender.update(lastFilmAdded);

            //Check the last element
            testReadLastElement(lastFilmAdded);
        }

        sender.close();
    }

    /**
     * JUnit tests to test the LoadArray function
     * @throws Exception
     */
    @Test
    public void testLoadArray() throws Exception {

        JGroupSender sender = new JGroupSender("defaultCluster", "sender");

        sender.open();

        ArrayList<Peer> list = sender.getListPeers();

        //Foreach peer, we add 2 elements
        for (Peer myPeer : list)
        {
            //Add a movie
            Film filmToAdd = new Film();
            filmToAdd.node_id = myPeer.getUid();
            filmToAdd.film_name = "Test JUnit";
            filmToAdd.duration = 10;
            filmToAdd.floatField = new Float(20);
            filmToAdd.longField = new Long(30);
            filmToAdd.creationDate = new Timestamp(10000);

            sender.add(filmToAdd);

            //Check the last element
            testReadLastElement(filmToAdd);

            Film lastFilmAdded = (Film)sender.readLastElement(filmToAdd);

            //Add 3 actors
            for(int i = 0; i < 3; i++)
            {
                Actor actorToAdd = new Actor();
                actorToAdd.node_id = myPeer.getUid();
                actorToAdd.actor_name = "actor "+i;
                actorToAdd.film_ID = lastFilmAdded.film_ID;

                sender.add(actorToAdd);
            }

            Actor actorToCheck = new Actor();
            actorToCheck.node_id = myPeer.getUid();

            //Check if the 3 elements have been added
            ArrayList<String> listCondition = new ArrayList<String>();
            listCondition.add("film_id = "+lastFilmAdded.film_ID);
            ArrayList<Actor> listActors = sender.listAll(actorToCheck, listCondition, null);
            assert listActors.size() == 3;

            //Test invalid parameters
            assert sender.loadArray(null, null, null) == null;

            assert sender.loadArray(lastFilmAdded, null, null) == null;

            assert sender.loadArray(lastFilmAdded, "", null) == null;

            //Loader load the array of actors
            lastFilmAdded = (Film)sender.loadArray(lastFilmAdded, "actors", "actor_id ASC");

            assert lastFilmAdded.actors.length == 3;

            //Check that the elements are corrects
            for(int i = 0; i < lastFilmAdded.actors.length; i++)
            {
                Actor anActor = lastFilmAdded.actors[i];
                assert anActor.actor_name.equals("actor "+i);
                assert anActor.film_ID == lastFilmAdded.film_ID;

                //Delete the actor
                sender.delete(anActor);
            }

            sender.delete(lastFilmAdded);
        }

        sender.close();
    }

    /**
     * JUnit tests to test the Delete function
     * @throws Exception
     */
    @Test
    public void testDelete() throws Exception {

        JGroupSender sender = new JGroupSender("defaultCluster", "sender");

        sender.open();

        //If there is no primary key, must return false
        assert sender.delete(new Film()) == false;

        ArrayList<Film> listFilms = sender.listAll(new Film(), null, null);

        //Delete all entities
        for(Film filmToDelete : listFilms)
        {
            sender.delete(filmToDelete);
        }

        //Check if all entities have been deleted
        assert sender.listAll(new Film(), null, null).size() == 0;

        sender.close();
    }
}
