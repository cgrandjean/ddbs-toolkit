package uk.co.cyril_grandjean.tests.junit;

import org.junit.Test;
import uk.co.cyril_grandjean.ddbstoolkit.module.datastore.mysql.DistributedMySQLTableManager;
import uk.co.cyril_grandjean.ddbstoolkit.module.datastore.mysql.MySQLConnector;
import uk.co.cyril_grandjean.tests.modelTestDatabase.Actor;
import uk.co.cyril_grandjean.tests.modelTestDatabase.Film;

import java.sql.Timestamp;
import java.util.ArrayList;

/**
 * JUnit tests to test DistributedMySQLTableManager class
 * User: Cyril GRANDJEAN
 * Date: 18/06/2012
 * Time: 16:04
 *
 * @version Creation of the class
 */
public class ADistributedMySQLTableManagerTest {

    /**
     * Test the connector
     * @throws Exception
     */
    @Test
    public void testIsOpen() throws Exception {

        DistributedMySQLTableManager manager = new DistributedMySQLTableManager(new MySQLConnector("jdbc:mysql://localhost:3306/ddbstoolkit", "root", "root")) ;
        manager.open();

        assert manager.isOpen() == true;

        manager.close();

        assert manager.isOpen() == false;
    }

    /**
     * JUnit tests for the add function of MySQL
     * @throws Exception
     */
    @Test
    public void testAdd() throws Exception {

        DistributedMySQLTableManager manager = new DistributedMySQLTableManager(new MySQLConnector("jdbc:mysql://localhost:3306/ddbstoolkit", "root", "root")) ;
        manager.open();

        int numberOfElement = manager.listAll(new Film(), null, null).size();

        //All parameters with no values
        Film filmToAdd = new Film();
        manager.add(filmToAdd);

        testReadLastElement(filmToAdd);

        numberOfElement++;

        //Add the string
        filmToAdd = new Film();
        filmToAdd.film_name = "Test JUnit 1";
        manager.add(filmToAdd);

        testReadLastElement(filmToAdd);

        numberOfElement++;

        //Add the duration
        filmToAdd = new Film();
        filmToAdd.film_name = "Test JUnit 2";
        filmToAdd.duration = 10;

        manager.add(filmToAdd);

        testReadLastElement(filmToAdd);

        numberOfElement++;

        //Add the float
        filmToAdd = new Film();
        filmToAdd.film_name = "Test JUnit 3";
        filmToAdd.duration = 10;
        filmToAdd.floatField = new Float(20);

        manager.add(filmToAdd);

        testReadLastElement(filmToAdd);

        numberOfElement++;

        //Add the long
        filmToAdd = new Film();
        filmToAdd.film_name = "Test JUnit 4";
        filmToAdd.duration = 10;
        filmToAdd.floatField = new Float(20);
        filmToAdd.longField = new Long(100);

        manager.add(filmToAdd);

        testReadLastElement(filmToAdd);

        numberOfElement++;

        //Add the timestamp
        filmToAdd = new Film();
        filmToAdd.film_name = "Test JUnit 5";
        filmToAdd.duration = 10;
        filmToAdd.floatField = new Float(20);
        filmToAdd.longField = new Long(100);
        filmToAdd.creationDate = new Timestamp(100000);

        manager.add(filmToAdd);

        testReadLastElement(filmToAdd);

        numberOfElement++;

        System.out.println(manager.listAll(new Film(), null, null).size());

        assert  manager.listAll(new Film(), null, null).size() == numberOfElement;

        manager.close();
    }

    /**
     * JUnit tests for the listAll function for MySQL
     * @throws Exception
     */
    @Test
    public void testListAll() throws Exception {

        DistributedMySQLTableManager manager = new DistributedMySQLTableManager(new MySQLConnector("jdbc:mysql://localhost:3306/ddbstoolkit", "root", "root")) ;
        manager.open();

        //All parameters : Must return null
        assert manager.listAll(null, null, null) == null;

        //Select a movie
        assert manager.listAll(new Film(), null, null) != null;

        //Select a movie order by filmID
        assert manager.listAll(new Film(), null, "film_ID ASC") != null;

        int numberOfElement = manager.listAll(new Film(), null, null).size();

        manager.close();
    }

    /**
     * JUnit tests to test the Read function
     * @throws Exception
     */
    @Test
    public void testRead() throws Exception {

        DistributedMySQLTableManager manager = new DistributedMySQLTableManager(new MySQLConnector("jdbc:mysql://localhost:3306/ddbstoolkit", "root", "root")) ;
        manager.open();

        //No object : must return null
        assert manager.read(null) == null;

        Film filmToRead = new Film();
        filmToRead.film_ID = -1;

        assert manager.read(filmToRead) == null;

        ArrayList<Film> listFilm = manager.listAll(new Film(), null, null);

        for(Film myFilm : listFilm)
        {
            //Read each film
            Film filmReaded = manager.read(myFilm);

            //Check the fields
            assert myFilm.film_name.equals(filmReaded.film_name);
            assert myFilm.duration == filmReaded.duration;
            assert myFilm.floatField == filmReaded.floatField;
            assert myFilm.longField == filmReaded.longField;
            assert myFilm.creationDate.equals(filmReaded.creationDate);
        }

        manager.close();
    }


    /**
     * JUnit tests to test that the last element correspond to the last element added
     * @param objectToCheck
     * @throws Exception
     */
    public void testReadLastElement(Film objectToCheck) throws Exception{

        DistributedMySQLTableManager manager = new DistributedMySQLTableManager(new MySQLConnector("jdbc:mysql://localhost:3306/ddbstoolkit", "root", "root")) ;
        manager.open();

        assert manager.readLastElement(null) == null;

        Film lastFilm = (Film)manager.readLastElement(new Film());

        System.out.println("objectToCheck "+objectToCheck.film_name+" lastFilm "+lastFilm.film_name);

        assert (objectToCheck.film_name == null || objectToCheck.film_name.equals(lastFilm.film_name));
        assert objectToCheck.duration == lastFilm.duration;
        assert objectToCheck.floatField == lastFilm.floatField;
        assert objectToCheck.longField == lastFilm.longField;
        assert objectToCheck.creationDate == null || objectToCheck.creationDate.equals(lastFilm.creationDate);

        manager.close();
    }

    /**
     * JUnit tests to test the update function
     * @throws Exception
     */
    @Test
    public void testUpdate() throws Exception {

        DistributedMySQLTableManager manager = new DistributedMySQLTableManager(new MySQLConnector("jdbc:mysql://localhost:3306/ddbstoolkit", "root", "root")) ;
        manager.open();

        //If there is no primary key, must return false
        assert manager.update(null) == false;

        //If there is no primary key, must return false
        assert manager.update(new Film()) == false;

        //Add a movie
        Film filmToAdd = new Film();
        filmToAdd.film_name = "Test JUnit";
        filmToAdd.duration = 10;
        filmToAdd.floatField = new Float(20);
        filmToAdd.longField = new Long(30);
        filmToAdd.creationDate = new Timestamp(10000);

        manager.add(filmToAdd);

        //Check the last element
        testReadLastElement(filmToAdd);

        //Modify the last element
        Film lastFilmAdded = (Film)manager.readLastElement(new Film());
        lastFilmAdded.film_name = "Test JUnit Updated";
        lastFilmAdded.duration = 20;
        lastFilmAdded.floatField = new Float(30);
        lastFilmAdded.longField = new Long(40);
        lastFilmAdded.creationDate = new Timestamp(20000);

        manager.update(lastFilmAdded);

        //Check the last element
        testReadLastElement(lastFilmAdded);

        manager.close();
    }

    /**
     * JUnit tests to test the Delete function
     * @throws Exception
     */
    @Test
    public void testDelete() throws Exception {

        DistributedMySQLTableManager manager = new DistributedMySQLTableManager(new MySQLConnector("jdbc:mysql://localhost:3306/ddbstoolkit", "root", "root")) ;
        manager.open();

        //If there is no primary key, must return false
        assert manager.delete(null) == false;

        //If there is no primary key, must return false
        assert manager.delete(new Film()) == false;

        ArrayList<Film> listEntities = manager.listAll(new Film(), null, null);

        //Delete all entities
        for(Film filmToDelete : listEntities)
        {
            manager.delete(filmToDelete);
        }

        //Check if all entities has been deleted
        assert manager.listAll(new Film(), null, null).size() == 0;

        manager.close();
    }

    /**
     * JUnit tests to test the LoadArray function
     * @throws Exception
     */
    @Test
    public void testLoadArray() throws Exception {

        DistributedMySQLTableManager manager = new DistributedMySQLTableManager(new MySQLConnector("jdbc:mysql://localhost:3306/ddbstoolkit", "root", "root")) ;
        manager.open();

        //Add a movie
        Film filmToAdd = new Film();
        filmToAdd.film_name = "Test JUnit";
        filmToAdd.duration = 10;
        filmToAdd.floatField = new Float(20);
        filmToAdd.longField = new Long(30);
        filmToAdd.creationDate = new Timestamp(10000);

        manager.add(filmToAdd);

        //Check the last element
        testReadLastElement(filmToAdd);

        Film lastFilmAdded = (Film)manager.readLastElement(new Film());

        //Add 3 actors
        for(int i = 0; i < 3; i++)
        {
            Actor actorToAdd = new Actor();
            actorToAdd.actor_name = "actor "+i;
            actorToAdd.film_ID = lastFilmAdded.film_ID;

            manager.add(actorToAdd);
        }

        //Check if the 3 elements have been added
        ArrayList<String> listCondition = new ArrayList<String>();
        listCondition.add("film_id = "+lastFilmAdded.film_ID);
        ArrayList<Actor> listActors = manager.listAll(new Actor(), listCondition, null);
        assert listActors.size() == 3;

        //Test invalid parameters
        assert manager.loadArray(null, null, null) == null;

        assert manager.loadArray(lastFilmAdded, null, null) == null;

        assert manager.loadArray(lastFilmAdded, "", null) == null;

        //Loader load the array of actors
        lastFilmAdded = (Film) manager.loadArray(lastFilmAdded, "actors", "actor_id ASC");

        assert lastFilmAdded.actors.length == 3;

        //Check that the elements are corrects
        for(int i = 0; i < lastFilmAdded.actors.length; i++)
        {
            Actor anActor = lastFilmAdded.actors[i];
            assert anActor.actor_name.equals("actor "+i);
            assert anActor.film_ID == lastFilmAdded.film_ID;

            //Delete the actor
            manager.delete(anActor);
        }

        manager.delete(lastFilmAdded);

        manager.close();
    }
}
