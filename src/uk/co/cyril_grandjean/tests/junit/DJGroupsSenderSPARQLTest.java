package uk.co.cyril_grandjean.tests.junit;

import org.junit.Test;
import uk.co.cyril_grandjean.ddbstoolkit.core.Peer;
import uk.co.cyril_grandjean.ddbstoolkit.module.middleware.jgroups.JGroupSender;
import uk.co.cyril_grandjean.tests.modelTestSPARQL.Company;
import uk.co.cyril_grandjean.tests.modelTestSPARQL.Employee;

import java.util.ArrayList;

/**
 * JUnit tests to test the JGroupSender class with SPARQL
 * User: Cyril GRANDJEAN
 * Date: 25/06/2012
 * Time: 15:42
 *
 * @version Creation of the class
 */
public class DJGroupsSenderSPARQLTest {

    /**
     * Test the connector
     * @throws Exception
     */
    @Test
    public void testIsOpen() throws Exception {

        JGroupSender sender = new JGroupSender("defaultCluster", "sender");

        sender.open();

        assert sender.isOpen() == true;

        sender.close();

        assert sender.isOpen() == false;
    }

    /**
     * JUnit tests for the list of peers
     * Condition 2 receivers must be launched
     * @throws Exception
     */
    @Test
    public void testGetListPeers() throws Exception {

        JGroupSender sender = new JGroupSender("defaultCluster", "sender");

        sender.open();

        ArrayList<Peer> list = sender.getListPeers();

        assert list.size() == 2;

        sender.close();
    }

    /**
     * JUnit tests for the listAll function for SPARQL
     * @throws Exception
     */
    @Test
    public void testListAll() throws Exception {


        JGroupSender sender = new JGroupSender("defaultCluster", "sender");

        sender.open();

        //All parameters : Must return null
        assert sender.listAll(null, null, null) == null;

        //Select a movie
        assert sender.listAll(new Company(), null, null) != null;

        int sizeList = sender.listAll(new Company(), null, null).size();

        ArrayList<Peer> list = sender.getListPeers();

        //Foreach peer, we add 2 elements
        for (Peer myPeer : list)
        {
            Company companyToAddLight = new Company();
            companyToAddLight.node_id = myPeer.getUid();
            companyToAddLight.company_uri = "http://cyril-grandjean.co.uk/business/CompanyTest";
            sender.add(companyToAddLight);

            sizeList++;

            //testReadLastElement(companyToAddLight);

            //Add a company
            Company companyToAdd = new Company();
            companyToAdd.node_id = myPeer.getUid();
            companyToAdd.company_uri = "http://cyril-grandjean.co.uk/business/Microsoft";
            companyToAdd.company_ID = 2;
            companyToAdd.company_name = "Microsoft";
            companyToAdd.floatField = 20;
            companyToAdd.longField = 30;
            companyToAdd.surface = 30;

            //Set 2 boss
            String[] boss = new String[2];
            boss[0] = "Bill Gates";
            boss[1] = "Steve Ballmer";
            companyToAdd.boss = boss;

            //Set 2 ints
            int[] mark = new int[2];
            mark[0] = 1;
            mark[1] = 2;
            companyToAdd.mark = mark;

            //Set 2 longs
            long[] number1 = new long[2];
            number1[0] = 3;
            number1[1] = 4;
            companyToAdd.number1 = number1;

            //Set 2 floats
            float[] number2 = new float[2];
            number2[0] = 5;
            number2[1] = 6;
            companyToAdd.number2 = number2;

            sender.add(companyToAdd);

            sizeList++;

            testReadLastElement(companyToAdd);
        }

        //Check that the size is 4
        assert sender.listAll(new Company(), null, null).size() == sizeList;


        sender.close();

    }

    /**
     * JUnit tests to test the Read function
     * @throws Exception
     */
    @Test
    public void testRead() throws Exception {

        JGroupSender sender = new JGroupSender("defaultCluster", "sender");

        sender.open();

        //No valid parameter, must be null
        assert sender.read(null) == null;

        //No valid parameter, must be null
        assert sender.read(new Company()) == null;

        ArrayList<Peer> list = sender.getListPeers();

        //Foreach peer, we add 2 elements
        for (Peer myPeer : list)
        {
            //Add a company
            Company companyToAdd = new Company();
            companyToAdd.node_id = myPeer.getUid();
            companyToAdd.company_uri = "http://cyril-grandjean.co.uk/business/Microsoft";
            companyToAdd.company_ID = 2;
            companyToAdd.company_name = "Microsoft";
            companyToAdd.floatField = 20;
            companyToAdd.longField = 30;
            companyToAdd.surface = 30;

            //Set 2 boss
            String[] boss = new String[2];
            boss[0] = "Bill Gates";
            boss[1] = "Steve Ballmer";
            companyToAdd.boss = boss;

            //Set 2 ints
            int[] mark = new int[2];
            mark[0] = 1;
            mark[1] = 2;
            companyToAdd.mark = mark;

            //Set 2 longs
            long[] number1 = new long[2];
            number1[0] = 3;
            number1[1] = 4;
            companyToAdd.number1 = number1;

            //Set 2 floats
            float[] number2 = new float[2];
            number2[0] = 5;
            number2[1] = 6;
            companyToAdd.number2 = number2;

            sender.add(companyToAdd);

            testReadLastElement(companyToAdd);

            Company lastFilmAdded = (Company)sender.readLastElement(companyToAdd);
            Company myCompany = (Company)sender.read(lastFilmAdded);

            assert companyToAdd.company_uri.equals(myCompany.company_uri);
            assert companyToAdd.company_ID == myCompany.company_ID;
            assert companyToAdd.company_name.equals(myCompany.company_name);
            assert companyToAdd.floatField == myCompany.floatField;
            assert companyToAdd.longField == myCompany.longField;
            assert companyToAdd.surface == myCompany.surface;

            for(int i = 0; i < companyToAdd.boss.length; i++)
            {
                assert companyToAdd.boss[i].equals(myCompany.boss[i]);
            }

            for(int i = 0; i < companyToAdd.mark.length; i++)
            {
                assert companyToAdd.mark[i] == myCompany.mark[i];
            }

            for(int i = 0; i < companyToAdd.number1.length; i++)
            {
                assert companyToAdd.number1[i] == myCompany.number1[i];
            }

            for(int i = 0; i < companyToAdd.number2.length; i++)
            {
                assert companyToAdd.number2[i] == myCompany.number2[i];
            }
        }

        sender.close();
    }

    /**
     * Check the last element
     * @param objectToCheck Object to check
     * @throws Exception
     */
    public void testReadLastElement(Company objectToCheck) throws Exception {

        JGroupSender sender = new JGroupSender("defaultCluster", "sender");

        sender.open();

        //No valid parameter, must be null
        assert sender.readLastElement(null) == null;

        //No valid parameter, must be null
        assert sender.readLastElement(new Company()) == null;

        Company myCompany = (Company)sender.readLastElement(objectToCheck);
        assert objectToCheck.company_uri.equals(myCompany.company_uri);
        assert objectToCheck.company_ID == myCompany.company_ID;
        assert objectToCheck.company_name.equals(myCompany.company_name);
        assert objectToCheck.floatField == myCompany.floatField;
        assert objectToCheck.longField == myCompany.longField;
        assert objectToCheck.surface == myCompany.surface;

        for(int i = 0; i < objectToCheck.boss.length; i++)
        {
            assert objectToCheck.boss[i].equals(myCompany.boss[i]);
        }

        for(int i = 0; i < objectToCheck.mark.length; i++)
        {
            assert objectToCheck.mark[i] == myCompany.mark[i];
        }

        for(int i = 0; i < objectToCheck.number1.length; i++)
        {
            assert objectToCheck.number1[i] == myCompany.number1[i];
        }

        for(int i = 0; i < objectToCheck.number2.length; i++)
        {
            assert objectToCheck.number2[i] == myCompany.number2[i];
        }

        sender.close();
    }

    /**
     * JUnit tests to test the Add function
     * @throws Exception
     */
    @Test
    public void testAdd() throws Exception {

        JGroupSender sender = new JGroupSender("defaultCluster", "sender");

        sender.open();

        //No valid parameter, must be null
        assert sender.add(null) == false;

        //No valid parameter, must be null
        assert sender.add(new Company()) == false;

        int numberOfElement = sender.listAll(new Company(), null, null).size();

        ArrayList<Peer> list = sender.getListPeers();

        //Foreach peer, we add 2 elements
        for (Peer myPeer : list)
        {
            Company companyToAddLight = new Company();
            companyToAddLight.node_id = myPeer.getUid();
            companyToAddLight.company_uri = "http://cyril-grandjean.co.uk/business/CompanyTest";
            sender.add(companyToAddLight);

            numberOfElement++;

            testReadLastElement(companyToAddLight);

            //Add a company
            Company companyToAdd = new Company();
            companyToAdd.node_id = myPeer.getUid();
            companyToAdd.company_uri = "http://cyril-grandjean.co.uk/business/Microsoft";
            companyToAdd.company_ID = 2;
            companyToAdd.company_name = "Microsoft";
            companyToAdd.floatField = 20;
            companyToAdd.longField = 30;
            companyToAdd.surface = 30;

            //Set 2 boss
            String[] boss = new String[2];
            boss[0] = "Bill Gates";
            boss[1] = "Steve Ballmer";
            companyToAdd.boss = boss;

            //Set 2 ints
            int[] mark = new int[2];
            mark[0] = 1;
            mark[1] = 2;
            companyToAdd.mark = mark;

            //Set 2 longs
            long[] number1 = new long[2];
            number1[0] = 3;
            number1[1] = 4;
            companyToAdd.number1 = number1;

            //Set 2 floats
            float[] number2 = new float[2];
            number2[0] = 5;
            number2[1] = 6;
            companyToAdd.number2 = number2;

            sender.add(companyToAdd);

            testReadLastElement(companyToAdd);

            numberOfElement++;
        }

        System.out.println(sender.listAll(new Company(), null, null).size());

        assert  sender.listAll(new Company(), null, null).size() == numberOfElement;
    }

    /**
     * JUnit tests to test the Update function
     * @throws Exception
     */
    @Test
    public void testUpdate() throws Exception {

        JGroupSender sender = new JGroupSender("defaultCluster", "sender");

        sender.open();

        //No valid parameter, must be null
        assert sender.update(null) == false;

        //No valid parameter, must be null
        assert sender.update(new Company()) == false;

        ArrayList<Peer> list = sender.getListPeers();

        //Foreach peer, we add 2 elements
        for (Peer myPeer : list)
        {
            //Add a company
            //Add a company
            Company companyToAdd = new Company();
            companyToAdd.node_id = myPeer.getUid();
            companyToAdd.company_uri = "http://cyril-grandjean.co.uk/business/Microsoft";
            companyToAdd.company_ID = 2;
            companyToAdd.company_name = "Microsoft";
            companyToAdd.floatField = 20;
            companyToAdd.longField = 30;
            companyToAdd.surface = 30;

            //Set 2 boss
            String[] boss = new String[2];
            boss[0] = "Bill Gates";
            boss[1] = "Steve Ballmer";
            companyToAdd.boss = boss;

            //Set 2 ints
            int[] mark = new int[2];
            mark[0] = 1;
            mark[1] = 2;
            companyToAdd.mark = mark;

            //Set 2 longs
            long[] number1 = new long[2];
            number1[0] = 3;
            number1[1] = 4;
            companyToAdd.number1 = number1;

            //Set 2 floats
            float[] number2 = new float[2];
            number2[0] = 5;
            number2[1] = 6;
            companyToAdd.number2 = number2;

            sender.add(companyToAdd);

            //Check the last element
            testReadLastElement(companyToAdd);

            //Modify the last element
            Company lastFilmAdded = (Company)sender.readLastElement(companyToAdd);
            lastFilmAdded.company_ID = 3;
            lastFilmAdded.company_name = "Microsoft Updated";
            lastFilmAdded.floatField = 30;
            lastFilmAdded.longField = 40;
            lastFilmAdded.surface = 50;

            //Set 2 boss
            boss = new String[2];
            boss[0] = "Bill Gates updated";
            boss[1] = "Steve Ballmer updated";
            lastFilmAdded.boss = boss;

            //Set 2 ints
            mark = new int[2];
            mark[0] = 3;
            mark[1] = 4;
            lastFilmAdded.mark = mark;

            //Set 2 longs
            number1 = new long[2];
            number1[0] = 5;
            number1[1] = 6;
            lastFilmAdded.number1 = number1;

            //Set 2 floats
            number2 = new float[2];
            number2[0] = 7;
            number2[1] = 8;
            lastFilmAdded.number2 = number2;

            sender.update(lastFilmAdded);

            //Check the last element
            testReadLastElement(lastFilmAdded);
        }

        sender.close();
    }

    /**
     * JUnit tests to test the LoadArray function
     * @throws Exception
     */
    @Test
    public void testLoadArray() throws Exception {

        JGroupSender sender = new JGroupSender("defaultCluster", "sender");

        sender.open();

        ArrayList<Peer> list = sender.getListPeers();
        int numberEmployees =  sender.listAll(new Employee(), null, null).size();

        //Foreach peer, we add 2 elements
        for (Peer myPeer : list)
        {
            //Add a company
            Company companyToAdd = new Company();
            companyToAdd.node_id = myPeer.getUid();
            companyToAdd.company_uri = "http://cyril-grandjean.co.uk/business/Microsoft";
            companyToAdd.company_ID = 2;
            companyToAdd.company_name = "Microsoft";
            companyToAdd.floatField = 20;
            companyToAdd.longField = 30;
            companyToAdd.surface = 30;

            //Set 2 boss
            String[] boss = new String[2];
            boss[0] = "Bill Gates";
            boss[1] = "Steve Ballmer";
            companyToAdd.boss = boss;

            //Set 2 employees
            Employee employee1 = new Employee();
            employee1.node_id = myPeer.getUid();
            employee1.employee_uri = "http://cyril-grandjean.co.uk/business/Cyril_Grandjean";
            employee1.name = "Cyril Grandjean";
            sender.add(employee1);

            numberEmployees++;

            Employee employee2 = new Employee();
            employee2.node_id = myPeer.getUid();
            employee2.employee_uri = "http://cyril-grandjean.co.uk/business/Steve_Jobs";
            employee2.name = "Steve Jobs";
            sender.add(employee2);

            numberEmployees++;

            ArrayList<Employee> listEmployees = sender.listAll(new Employee(), null, null);
            assert listEmployees.size() == numberEmployees;

            Employee[] listEmployee = new Employee[2];
            companyToAdd.employee = listEmployee;
            companyToAdd.employee[0] = employee1;
            companyToAdd.employee[1] = employee2;

            //Add the company
            sender.add(companyToAdd);

            Company companyToLoad = (Company)sender.loadArray(companyToAdd, "employee", null);

            Company lastCompanyAdded = (Company)sender.readLastElement(new Company());

            //Loader load the array of actors
            lastCompanyAdded = (Company)sender.loadArray(lastCompanyAdded, "actors", "actor_id ASC");

            assert lastCompanyAdded.employee.length == 2;

            for (int i = 0; i < companyToLoad.employee.length; i++)
            {
                assert companyToLoad.employee[i].employee_uri.equals(employee1.employee_uri) || companyToLoad.employee[i].employee_uri.equals(employee2.employee_uri);
                assert companyToLoad.employee[i].name.equals(employee1.name) || companyToLoad.employee[i].name.equals(employee2.name);

                sender.delete(companyToLoad.employee[i]);
            }

            //Test invalid parameters
            assert sender.loadArray(null, null, null) == null;

            assert sender.loadArray(companyToLoad, null, null) == null;

            assert sender.loadArray(companyToLoad, "", null) == null;

            sender.delete(lastCompanyAdded);
        }

        sender.close();
    }

    /**
     * JUnit tests to test the Delete function
     * @throws Exception
     */
    @Test
    public void testDelete() throws Exception {

        JGroupSender sender = new JGroupSender("defaultCluster", "sender");

        sender.open();

        //If there is no primary key, must return false
        assert sender.delete(new Company()) == false;

        ArrayList<Company> listEntities = sender.listAll(new Company(), null, null);

        //Delete all entities
        for(Company myCompany : listEntities)
        {
            sender.delete(myCompany);
        }

        //Check if all entities have been deleted
        assert sender.listAll(new Company(), null, null).size() == 0;

        sender.close();
    }
}
