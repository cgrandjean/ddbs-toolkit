package uk.co.cyril_grandjean.tests.junit;

import com.hp.hpl.jena.query.*;
import com.hp.hpl.jena.tdb.TDBFactory;
import org.junit.Test;
import uk.co.cyril_grandjean.ddbstoolkit.core.Peer;
import uk.co.cyril_grandjean.ddbstoolkit.module.datastore.mysql.DistributedMySQLTableManager;
import uk.co.cyril_grandjean.ddbstoolkit.module.datastore.mysql.MySQLConnector;
import uk.co.cyril_grandjean.ddbstoolkit.module.datastore.sparql.DistributedSPARQLManager;
import uk.co.cyril_grandjean.ddbstoolkit.module.datastore.sqlite.DistributedSQLiteTableManager;
import uk.co.cyril_grandjean.ddbstoolkit.module.datastore.sqlite.SQLiteConnector;
import uk.co.cyril_grandjean.ddbstoolkit.module.middleware.jgroups.JGroupSender;
import uk.co.cyril_grandjean.ddbstoolkit.module.middleware.sqlspaces.SqlSpacesSender;
import uk.co.cyril_grandjean.tests.modelTestPerformances.Actor;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;

/**
 * JUnit class to test the performances of each modules
 * User: Cyril GRANDJEAN
 * Date: 20/07/2012
 * Time: 14:56
 *
 * @version Creation of the class
 */
public class GPerformancesTests {

    /**
     * Test the SPARQL performance
     * @throws Exception
     */
    @Test
    public void SparqlPerformances() throws Exception {

        System.out.println("Performances SPARQL");

        long startTime;

        //Add 1000 objects for the test
        DistributedSPARQLManager manager = new DistributedSPARQLManager(new Peer(), "/Users/cyril/Desktop/datastore");
        manager.open();

        Actor actorToAdd = new Actor();
        actorToAdd.actor_name = "Cyril";

        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(new File("/Users/cyril/Desktop/addModuleSPARQL.csv"), true));

            //Add 1000 objects
            for(int i = 0; i < 1000; i++)
            {
                actorToAdd.actor_id = i;
                actorToAdd.actor_uri = "http://www.cyril-grandjean.co.uk/actors/Cyril" +i;

                startTime = System.nanoTime();

                manager.add(actorToAdd);

                bw.write(String.valueOf(System.nanoTime() - startTime));

                bw.newLine();
            }


            bw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        manager.close();

        Dataset myDataset = TDBFactory.createDataset("/Users/cyril/Desktop/datastore");

        //List the 1000 objects
        System.out.println("List 1000 objects  ");

        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(new File(
                    "/Users/cyril/Desktop/listAllSPARQL.csv"), true));

            //Listl 1000 objects 10 times
            for(int i = 0; i < 10; i++)
            {
                startTime = System.nanoTime();

                myDataset.begin(ReadWrite.READ);

                Query query = QueryFactory.create("prefix fb: <http://rdf.freebase.com/ns/> SELECT ?actor_uri ?actor_id ?actor_name WHERE { OPTIONAL { ?actor_uri fb:actor_id ?actor_id} . ?actor_uri fb:actor_name ?actor_name }");

                try {
                    QueryExecution qe = QueryExecutionFactory.create(query, myDataset.getDefaultModel());
                    ResultSet results = qe.execSelect();
                }
                finally
                {
                    myDataset.end() ;
                }

                bw.write(String.valueOf(System.nanoTime() - startTime));

                bw.newLine();
            }


            bw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("Read 1 object");

        Actor actorToRead = new Actor();
        actorToRead.actor_id = 1;

        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(new File(
                    "/Users/cyril/Desktop/readSPARQL.csv"), true));

            //Read 1000 objects
            for(int i = 0; i < 1000; i++)
            {
                startTime = System.nanoTime();

                myDataset.begin(ReadWrite.READ);

                Query query = QueryFactory.create("prefix fb: <http://rdf.freebase.com/ns/> SELECT  ?actor_id ?actor_name WHERE { OPTIONAL { <http://www.cyril-grandjean.co.uk/actors/Cyril> fb:actor_id ?actor_id} . <http://www.cyril-grandjean.co.uk/actors/Cyril> fb:actor_name ?actor_name}");

                try {
                    QueryExecution qe = QueryExecutionFactory.create(query, myDataset.getDefaultModel());
                    ResultSet results = qe.execSelect();
                }
                finally
                {
                    myDataset.end() ;
                }

                bw.write(String.valueOf(System.nanoTime() - startTime));

                bw.newLine();
            }


            bw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //Delete objects used for the test
        manager = new DistributedSPARQLManager(new Peer(), "/Users/cyril/Desktop/datastore");
        manager.open();

        //Delete all objects of the table
        ArrayList<Actor> listEntities = manager.listAll(new Actor(), null, null);

        //Delete all entities
        for(Actor actorToDelete : listEntities)
        {
            manager.delete(actorToDelete);
        }

        manager.close();

    }

    /**
     * Test the MySQL module performance
     * @throws Exception
     */
    @Test
    public void mySQLModulePerformances() throws Exception {

        System.out.println("Performances MySQL modules");

        DistributedMySQLTableManager manager = new DistributedMySQLTableManager(new MySQLConnector("jdbc:mysql://localhost:3306/performancesTest", "root", "root")) ;
        manager.open();

        long startTime;

        //Delete all objects of the table
        ArrayList<Actor> listEntities = manager.listAll(new Actor(), null, null);

        //Delete all entities
        for(Actor actorToDelete : listEntities)
        {
            manager.delete(actorToDelete);
        }


        System.out.println("Add 1000 objects");

        Actor actorToAdd = new Actor();
        actorToAdd.actor_name = "Cyril";

        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(new File(
                    "/Users/cyril/Desktop/addModuleMySQL.csv"), true));

            //Add 1000 objects
            for(int i = 0; i < 1000; i++)
            {
                actorToAdd.actor_uri = "http://www.cyril-grandjean.co.uk/actors/Cyril" +i;

                startTime = System.nanoTime();

                manager.add(actorToAdd);

                bw.write(String.valueOf(System.nanoTime() - startTime));

                bw.newLine();
            }


            bw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //List the 1000 objects
        System.out.println("List 1000 objects");

        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(new File(
                    "/Users/cyril/Desktop/listAllModuleMySQL.csv"), true));

            //ListAll 1000 objects 1000 times
            for(int i = 0; i < 10; i++)
            {
                startTime = System.nanoTime();

                listEntities = manager.listAll(new Actor(), null, null);

                bw.write(String.valueOf(System.nanoTime() - startTime));

                bw.newLine();
            }


            bw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("Read 1 object");

        Actor actorToRead = new Actor();
        actorToRead.actor_id = 1;

        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(new File(
                    "/Users/cyril/Desktop/readModuleMySQL.csv"), true));

            //Read 1000 objects
            for(int i = 0; i < 1000; i++)
            {
                startTime = System.nanoTime();

                manager.read(actorToRead);

                bw.write(String.valueOf(System.nanoTime() - startTime));

                bw.newLine();
            }


            bw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //Update the 1000 objects of the table
        System.out.println("Update the 1000 objects : ");

        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(new File(
                    "/Users/cyril/Desktop/updateModuleMySQL.csv"), true));

            for(Actor actorToUpdate : listEntities)
            {
                actorToUpdate.actor_name += "U";
                actorToUpdate.actor_uri += "U";

                startTime = System.nanoTime();

                manager.update(actorToUpdate);

                bw.write(String.valueOf(System.nanoTime() - startTime));

                bw.newLine();
            }


            bw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //Delete the 1000 objects of the table
        System.out.println("Delete the 1000 objects : ");

        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(new File(
                    "/Users/cyril/Desktop/deleteModuleMySQL.csv"), true));

            listEntities = manager.listAll(new Actor(), null, null);

            for(Actor actorToDelete : listEntities)
            {
                startTime = System.nanoTime();

                manager.delete(actorToDelete);

                bw.write(String.valueOf(System.nanoTime() - startTime));

                bw.newLine();
            }


            bw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        manager.close();
    }

    /**
     * Test the performance of the SPARQL module
     * @throws Exception
     */
    @Test
    public void SparqlModulePerformances() throws Exception {

        System.out.println("Performances SPARQL modules");

        DistributedSPARQLManager manager = new DistributedSPARQLManager(new Peer(), "/Users/cyril/Desktop/datastore");
        manager.open();

        long startTime;

        //Delete all objects of the table
        ArrayList<Actor> listEntities = manager.listAll(new Actor(), null, null);

        //Delete all entities
        for(Actor myActor : listEntities)
        {
            manager.delete(myActor);
        }


        System.out.println("Add 1000 objects");

        Actor actorToAdd = new Actor();
        actorToAdd.actor_name = "Cyril";

        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(new File(
                    "/Users/cyril/Desktop/addModuleSPARQL.csv"), true));

            //Add 1000 objects
            for(int i = 0; i < 1000; i++)
            {
                actorToAdd.actor_id = i;
                actorToAdd.actor_uri = "http://www.cyril-grandjean.co.uk/actors/Cyril" +i;

                startTime = System.nanoTime();

                manager.add(actorToAdd);

                bw.write(String.valueOf(System.nanoTime() - startTime));

                bw.newLine();
            }


            bw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //List the 1000 objects
        System.out.println("List 1000 objects  ");

        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(new File(
                    "/Users/cyril/Desktop/listAllModuleSPARQL.csv"), true));

            //ListAll 1000 objects 10 times
            for(int i = 0; i < 10; i++)
            {
                startTime = System.nanoTime();

                listEntities = manager.listAll(new Actor(), null, null);

                bw.write(String.valueOf(System.nanoTime() - startTime));

                bw.newLine();
            }


            bw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("Read 1 object");

        Actor actorToRead = new Actor();
        actorToRead.actor_id = 1;

        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(new File(
                    "/Users/cyril/Desktop/readModuleSPARQL.csv"), true));

            //Read 1000 objects
            for(int i = 0; i < 1000; i++)
            {
                startTime = System.nanoTime();

                manager.read(actorToRead);

                bw.write(String.valueOf(System.nanoTime() - startTime));

                bw.newLine();
            }


            bw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //Update the 1000 objects of the table
        System.out.println("Update the 1000 objects : ");

        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(new File(
                    "/Users/cyril/Desktop/updateModuleSPARQL.csv"), true));

            for(Actor actorToUpdate : listEntities)
            {
                actorToUpdate.actor_name += "U";
                actorToUpdate.actor_id = actorToUpdate.actor_id+1;

                startTime = System.nanoTime();

                manager.update(actorToUpdate);

                bw.write(String.valueOf(System.nanoTime() - startTime));

                bw.newLine();
            }


            bw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //Delete the 1000 objects of the table
        System.out.print("Delete the 1000 objects : ");

        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(new File(
                    "/Users/cyril/Desktop/deleteModuleSPARQL.csv"), true));

            for(Actor myActor : listEntities)
            {
                startTime = System.nanoTime();

                manager.delete(myActor);

                bw.write(String.valueOf(System.nanoTime() - startTime));

                bw.newLine();
            }


            bw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        manager.close();
    }

    /**
     * Test the performance of the JGroups module with MySQL
     * @throws Exception
     */
    @Test
    public void JGroupsMySQLPerformances() throws Exception {

        System.out.println("Performances JGroups with MySQL modules");

        JGroupSender sender = new JGroupSender("defaultCluster", "sender");

        sender.open();

        long startTime;

        //Delete all objects of the table
        ArrayList<Actor> listEntities = sender.listAll(new Actor(), null, null);

        //Delete all entities
        for(Actor myActors : listEntities)
        {
            sender.delete(myActors);
        }

        Peer peerReceiver = sender.getListPeers().get(0);

        System.out.println("Add 1000 objects");

        Actor actorToAdd = new Actor();
        actorToAdd.node_id = peerReceiver.getUid();
        actorToAdd.actor_name = "Cyril";

        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(new File(
                    "/Users/cyril/Desktop/addModuleJGroupsMySQL.csv"), true));

            //Add 1000 objects
            for(int i = 0; i < 1000; i++)
            {
                actorToAdd.actor_uri = "http://www.cyril-grandjean.co.uk/actors/Cyril" +i;

                startTime = System.nanoTime();

                sender.add(actorToAdd);

                bw.write(String.valueOf(System.nanoTime() - startTime));

                bw.newLine();
            }


            bw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //List the 1000 objects
        System.out.println("List 1000 objects ");

        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(new File(
                    "/Users/cyril/Desktop/listAllModuleJGroupsMySQL.csv"), true));

            //List 1000 objects 10 times
            for(int i = 0; i < 10; i++)
            {
                startTime = System.nanoTime();

                listEntities = sender.listAll(new Actor(), null, null);

                bw.write(String.valueOf(System.nanoTime() - startTime));

                bw.newLine();
            }


            bw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("Read 1 object");

        Actor actorToRead = new Actor();
        actorToRead.node_id = peerReceiver.getUid();
        actorToRead.actor_id = 1;

        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(new File(
                    "/Users/cyril/Desktop/readModuleJGroupsMySQL.csv"), true));

            //Read 1000 objects
            for(int i = 0; i < 1000; i++)
            {
                startTime = System.nanoTime();

                sender.read(actorToRead);

                bw.write(String.valueOf(System.nanoTime() - startTime));

                bw.newLine();
            }


            bw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //Update the 1000 objects of the table
        System.out.println("Update the 1000 objects");

        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(new File(
                    "/Users/cyril/Desktop/updateModuleMySQL.csv"), true));

            for(Actor actorToUpdate : listEntities)
            {
                actorToUpdate.actor_name += "U";
                actorToUpdate.actor_uri += "U";

                startTime = System.nanoTime();

                sender.update(actorToUpdate);

                bw.write(String.valueOf(System.nanoTime() - startTime));

                bw.newLine();
            }


            bw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //Delete the 1000 objects of the table
        System.out.println("Delete the 1000 objects");

        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(new File(
                    "/Users/cyril/Desktop/deleteModuleJGroupsMySQL.csv"), true));

            for(Actor myActor : listEntities)
            {
                startTime = System.nanoTime();

                sender.delete(myActor);

                bw.write(String.valueOf(System.nanoTime() - startTime));

                bw.newLine();
            }


            bw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        sender.close();
    }

    /**
     * Test the performance of the JGroups module with SPARQL
     * @throws Exception
     */
    @Test
    public void JGroupsSPARQLPerformances() throws Exception {

        System.out.println("Performances JGroups with SPARQL modules");

        JGroupSender sender = new JGroupSender("defaultCluster", "sender");

        sender.open();

        long startTime;

        //Delete all objects of the table
        ArrayList<Actor> listEntities = sender.listAll(new Actor(), null, null);

        //Delete all entities
        for(Actor actorToDelete : listEntities)
        {
            sender.delete(actorToDelete);
        }

        Peer peerReceiver = sender.getListPeers().get(0);

        System.out.println("Add 1000 objects");

        Actor actorToAdd = new Actor();
        actorToAdd.node_id = peerReceiver.getUid();
        actorToAdd.actor_name = "Cyril";

        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(new File(
                    "/Users/cyril/Desktop/addModuleJgroupsSPARQL.csv"), true));

            //Add 1000 objects
            for(int i = 0; i < 1000; i++)
            {
                actorToAdd.actor_id = i;
                actorToAdd.actor_uri = "http://www.cyril-grandjean.co.uk/actors/Cyril" +i;

                startTime = System.nanoTime();

                sender.add(actorToAdd);

                bw.write(String.valueOf(System.nanoTime() - startTime));

                bw.newLine();
            }


            bw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //List the 1000 objects
        System.out.println("List 1000 objects");

        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(new File(
                    "/Users/cyril/Desktop/listAllModuleJgroupsSPARQL.csv"), true));

            //List 1000 objects 10 times
            for(int i = 0; i < 10; i++)
            {
                startTime = System.nanoTime();

                listEntities = sender.listAll(new Actor(), null, null);

                bw.write(String.valueOf(System.nanoTime() - startTime));

                bw.newLine();
            }


            bw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("Read 1 object");

        Actor actorToRead = new Actor();
        actorToRead.node_id = peerReceiver.getUid();
        actorToRead.actor_id = 1;

        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(new File(
                    "/Users/cyril/Desktop/readModuleJgroupsSPARQL.csv"), true));

            //Read 1000 objects
            for(int i = 0; i < 1000; i++)
            {
                startTime = System.nanoTime();

                sender.read(actorToRead);

                bw.write(String.valueOf(System.nanoTime() - startTime));

                bw.newLine();
            }


            bw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //Update the 1000 objects of the table
        System.out.println("Update the 1000 objects");

        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(new File(
                    "/Users/cyril/Desktop/updateModuleSQLSpacesSPARQL.csv"), true));

            for(Actor actorToUpdate : listEntities)
            {
                actorToUpdate.actor_id = actorToUpdate.actor_id + 1;

                startTime = System.nanoTime();

                sender.update(actorToUpdate);

                bw.write(String.valueOf(System.nanoTime() - startTime));

                bw.newLine();
            }


            bw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //Delete the 1000 objects of the table
        System.out.println("Delete the 1000 objects");

        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(new File(
                    "/Users/cyril/Desktop/deleteModuleJgroupsSPARQL.csv"), true));

            for(Actor actorToDelete : listEntities)
            {
                startTime = System.nanoTime();

                sender.delete(actorToDelete);

                bw.write(String.valueOf(System.nanoTime() - startTime));

                bw.newLine();
            }


            bw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        sender.close();
    }

    /**
     * Test the performance of the SQLSpaces module with MySQL
     * @throws Exception
     */
    @Test
    public void SQLSpacesMySQLPerformances() throws Exception {

        System.out.println("Performances SqlSpaces with MySQL modules");

        SqlSpacesSender sender = new SqlSpacesSender("defaultCluster", "sender");

        sender.open();

        long startTime;

        Peer peerReceiver = sender.getListPeers().get(0);

        //Delete all objects of the table
        ArrayList<Actor> listEntities = sender.listAll(new Actor(), null, null);

        //Delete all entities
        for(Actor actorToDelete : listEntities)
        {
            sender.delete(actorToDelete);
        }

        System.out.println("Add 1000 objects");

        Actor actorToAdd = new Actor();
        actorToAdd.node_id = peerReceiver.getUid();
        actorToAdd.actor_name = "Cyril";

        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(new File(
                    "/Users/cyril/Desktop/addModuleSqlSpacesMySQL.csv"), true));

            //Add 1000 objects
            for(int i = 0; i < 1000; i++)
            {
                actorToAdd.actor_uri = "http://www.cyril-grandjean.co.uk/actors/Cyril" +i;

                startTime = System.nanoTime();

                sender.add(actorToAdd);

                bw.write(String.valueOf(System.nanoTime() - startTime));

                bw.newLine();
            }


            bw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //List the 1000 objects
        System.out.println("List 1000 objects");

        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(new File(
                    "/Users/cyril/Desktop/listAllModuleSqlSpacesMySQL.csv"), true));

            //List 1000 objects 10 times
            for(int i = 0; i < 10; i++)
            {
                startTime = System.nanoTime();

                listEntities = sender.listAll(new Actor(), null, null);

                bw.write(String.valueOf(System.nanoTime() - startTime));

                bw.newLine();
            }


            bw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("Read 1 object");

        Actor actorToRead = new Actor();
        actorToRead.node_id = peerReceiver.getUid();
        actorToRead.actor_id = 1;

        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(new File(
                    "/Users/cyril/Desktop/readModuleSqlSpacesMySQL.csv"), true));

            //Read 1000 objects
            for(int i = 0; i < 1000; i++)
            {
                System.out.println("Read object "+i);

                startTime = System.nanoTime();

                sender.read(actorToRead);

                bw.write(String.valueOf(System.nanoTime() - startTime));

                bw.newLine();
            }


            bw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //Update the 1000 objects of the table
        System.out.println("Update the 1000 objects");

        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(new File(
                    "/Users/cyril/Desktop/updateModuleSQLSpacesMySQL.csv"), true));

            int i = 0;
            for(Actor actorToUpdate : listEntities)
            {
                actorToUpdate.actor_name += "U";
                actorToUpdate.actor_uri += "U";

                System.out.println("Update object "+i);

                startTime = System.nanoTime();

                sender.update(actorToUpdate);

                bw.write(String.valueOf(System.nanoTime() - startTime));

                bw.newLine();

                i++;
            }


            bw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //Delete the 1000 objects of the table
        System.out.print("Delete the 1000 objects : ");

        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(new File(
                    "/Users/cyril/Desktop/deleteModuleSqlSpacesMySQL.csv"), true));

            int i = 0;
            for(Actor actorToDelete : listEntities)
            {
                System.out.println("Delete object "+i);

                startTime = System.nanoTime();

                sender.delete(actorToDelete);

                bw.write(String.valueOf(System.nanoTime() - startTime));

                bw.newLine();

                i++;
            }


            bw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        sender.close();
    }

    /**
     * Test the performance of the SQLSpaces module with SPARQL
     * @throws Exception
     */
    @Test
    public void SQLSpacesSPARQLPerformances() throws Exception {

        System.out.println("Performances SqlSpaces with SPARQL modules");

        SqlSpacesSender sender = new SqlSpacesSender("defaultCluster", "sender");

        sender.open();

        long startTime;

        Peer peerReceiver = sender.getListPeers().get(0);

        //Delete all objects of the table
        ArrayList<Actor> listEntities = sender.listAll(new Actor(), null, null);

        //Delete all entities
        for(Actor actorToDelete : listEntities)
        {
            sender.delete(actorToDelete);
        }

        System.out.println("Add 1000 objects");

        Actor actorToAdd = new Actor();
        actorToAdd.node_id = peerReceiver.getUid();
        actorToAdd.actor_name = "Cyril";

        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(new File(
                    "/Users/cyril/Desktop/addModuleSqlSpacesSPARQL.csv"), true));

            //Add 1000 objects
            for(int i = 0; i < 1000; i++)
            {
                actorToAdd.actor_id = i;
                actorToAdd.actor_uri = "http://www.cyril-grandjean.co.uk/actors/Cyril" +i;

                startTime = System.nanoTime();

                sender.add(actorToAdd);

                bw.write(String.valueOf(System.nanoTime() - startTime));

                bw.newLine();
            }


            bw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //List the 1000 objects
        System.out.println("List 1000 objects");

        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(new File(
                    "/Users/cyril/Desktop/listAllModuleSqlSpacesSPARQL.csv"), true));

            //List 1000 objects 10 times
            for(int i = 0; i < 10; i++)
            {
                startTime = System.nanoTime();

                listEntities = sender.listAll(new Actor(), null, null);

                bw.write(String.valueOf(System.nanoTime() - startTime));

                bw.newLine();
            }


            bw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }


        System.out.println("Read 1 object");

        Actor actorToRead = new Actor();
        actorToRead.node_id = peerReceiver.getUid();
        actorToRead.actor_id = 1;

        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(new File(
                    "/Users/cyril/Desktop/readModuleSqlSpacesSPARQL.csv"), true));

            //Read 1000 objects
            for(int i = 0; i < 1000; i++)
            {
                startTime = System.nanoTime();

                sender.read(actorToRead);

                bw.write(String.valueOf(System.nanoTime() - startTime));

                bw.newLine();
            }


            bw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //Update the 1000 objects of the table
        System.out.println("Update the 1000 objects");

        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(new File(
                    "/Users/cyril/Desktop/updateModuleSQLSpacesSPARQL.csv"), true));

            for(Actor actorToUpdate : listEntities)
            {
                actorToUpdate.actor_id = actorToUpdate.actor_id + 1;

                startTime = System.nanoTime();

                sender.update(actorToUpdate);

                bw.write(String.valueOf(System.nanoTime() - startTime));

                bw.newLine();
            }


            bw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //Delete the 1000 objects of the table
        System.out.println("Delete the 1000 objects");

        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(new File(
                    "/Users/cyril/Desktop/deleteModuleSqlSpacesSPARQL.csv"), true));

            for(Actor actorToDelete : listEntities)
            {
                startTime = System.nanoTime();

                sender.delete(actorToDelete);

                bw.write(String.valueOf(System.nanoTime() - startTime));

                bw.newLine();
            }


            bw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        sender.close();
    }

    /**
     * Test the MySQL module performance
     * @throws Exception
     */
    @Test
    public void sqliteModulePerformances() throws Exception {

        System.out.println("Performances SQLLite modules");

        SQLiteConnector connector = new SQLiteConnector("/Users/Cyril/Desktop/", "testPerformance.db");

        String actorTable = "CREATE TABLE Actor (actor_id INTEGER NOT NULL, actor_uri VARCHAR(25), actor_name VARCHAR(25), PRIMARY KEY (actor_id));";

        connector.open();
        connector.executeQuery(actorTable);
        connector.close();

        DistributedSQLiteTableManager manager = new DistributedSQLiteTableManager(connector) ;
        manager.open();

        long startTime;

        //Delete all objects of the table
        ArrayList<Actor> listEntities = manager.listAll(new Actor(), null, null);

        //Delete all entities
        for(Actor actorToDelete : listEntities)
        {
            manager.delete(actorToDelete);
        }


        System.out.println("Add 1000 objects");

        Actor actorToAdd = new Actor();
        actorToAdd.actor_name = "Cyril";

        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(new File(
                    "/Users/cyril/Desktop/addModuleSQLite.csv"), true));

            //Add 1000 objects
            for(int i = 0; i < 1000; i++)
            {
                actorToAdd.actor_uri = "http://www.cyril-grandjean.co.uk/actors/Cyril" +i;

                startTime = System.nanoTime();

                manager.add(actorToAdd);

                bw.write(String.valueOf(System.nanoTime() - startTime));

                bw.newLine();
            }


            bw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //List the 1000 objects
        System.out.println("List 1000 objects");

        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(new File(
                    "/Users/cyril/Desktop/listAllModuleSQLite.csv"), true));

            //ListAll 1000 objects 1000 times
            for(int i = 0; i < 10; i++)
            {
                startTime = System.nanoTime();

                listEntities = manager.listAll(new Actor(), null, null);

                bw.write(String.valueOf(System.nanoTime() - startTime));

                bw.newLine();
            }


            bw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("Read 1 object");

        Actor actorToRead = new Actor();
        actorToRead.actor_id = 1;

        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(new File(
                    "/Users/cyril/Desktop/readModuleSQLite.csv"), true));

            //Read 1000 objects
            for(int i = 0; i < 1000; i++)
            {
                startTime = System.nanoTime();

                manager.read(actorToRead);

                bw.write(String.valueOf(System.nanoTime() - startTime));

                bw.newLine();
            }


            bw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //Update the 1000 objects of the table
        System.out.println("Update the 1000 objects : ");

        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(new File(
                    "/Users/cyril/Desktop/updateModuleSQLite.csv"), true));

            for(Actor actorToUpdate : listEntities)
            {
                actorToUpdate.actor_name += "U";
                actorToUpdate.actor_uri += "U";

                startTime = System.nanoTime();

                manager.update(actorToUpdate);

                bw.write(String.valueOf(System.nanoTime() - startTime));

                bw.newLine();
            }


            bw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //Delete the 1000 objects of the table
        System.out.println("Delete the 1000 objects : ");

        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(new File(
                    "/Users/cyril/Desktop/deleteModuleSQLite.csv"), true));

            listEntities = manager.listAll(new Actor(), null, null);

            for(Actor actorToDelete : listEntities)
            {
                startTime = System.nanoTime();

                manager.delete(actorToDelete);

                bw.write(String.valueOf(System.nanoTime() - startTime));

                bw.newLine();
            }


            bw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        manager.close();
    }

}
