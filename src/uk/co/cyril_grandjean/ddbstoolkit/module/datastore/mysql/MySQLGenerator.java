package uk.co.cyril_grandjean.ddbstoolkit.module.datastore.mysql;

import uk.co.cyril_grandjean.ddbstoolkit.core.ClassInspector;
import uk.co.cyril_grandjean.ddbstoolkit.core.ClassProperty;
import uk.co.cyril_grandjean.ddbstoolkit.core.GeneratableNonReflectedEntity;
import uk.co.cyril_grandjean.ddbstoolkit.core.IEntity;
//import uk.co.cyril_grandjean.tests.modelTestDatabase.Actor;
//import uk.co.cyril_grandjean.tests.modelTestDatabase.Film;

import java.lang.reflect.Field;
import java.util.ArrayList;

/**
 * Generate a non reflected Entity
 * Created with IntelliJ IDEA.
 * User: cgrandjean
 * Date: 26/09/13
 * Time: 10:18
 */
public class MySQLGenerator implements GeneratableNonReflectedEntity {

    @Override
    public void generateConversionResultSetMethod(IEntity entity) throws ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchFieldException {

        StringBuilder stringMethod = new StringBuilder();
        stringMethod.append("@Override\n");
        stringMethod.append("public <T extends IEntity> ArrayList<T> conversionResultSet(ResultSet results, T myObject) throws Exception {\n\n");

        stringMethod.append("\tArrayList<T> resultList = new ArrayList<>();\n");

        stringMethod.append("\n\twhile(results.next()){\n");

        stringMethod.append("\n\t\t"+ClassInspector.getClassName(entity)+" entity = new "+ClassInspector.getClassName(entity)+"();\n");

        //Get class name
        String nameClass = ClassInspector.getFullClassName(entity);

        //List properties
        ArrayList<ClassProperty> listProperties = ClassInspector.exploreProperties(entity);

        Object myData = Class.forName(nameClass).newInstance();

        //Set object properties
        for(ClassProperty myProperty : listProperties)
        {
            Field f = myData.getClass().getField(myProperty.getName());

            //If it's not an array
            if(!myProperty.isArray())
            {
                if(myProperty.getType().equals("int"))
                {
                    stringMethod.append("\t\tentity."+myProperty.getName()+" = results.getInt(\""+myProperty.getPropertyName()+"\");\n");
                }
                else if(myProperty.getType().equals("long"))
                {
                    stringMethod.append("\t\tentity."+myProperty.getName()+" = results.getLong(\""+myProperty.getPropertyName()+"\");\n");
                }
                else if(myProperty.getType().equals("float"))
                {
                    stringMethod.append("\t\tentity."+myProperty.getName()+" = results.getFloat(\""+myProperty.getPropertyName()+"\");\n");
                }
                else if(myProperty.getType().equals("java.lang.String"))
                {
                    //If it's the node_id property
                    //TODO
                    if(myProperty.getPropertyName().equals("node_id"))
                    {
                    }
                    else
                    {
                        stringMethod.append("\t\tentity."+myProperty.getName()+" = results.getString(\""+myProperty.getPropertyName()+"\");\n");
                    }
                }
                else if(myProperty.getType().equals("java.sql.Timestamp"))
                {
                    stringMethod.append("\t\tentity."+myProperty.getName()+" = results.getTimestamp(\""+myProperty.getPropertyName()+"\");\n");
                }
            }
        }

        stringMethod.append("\n\t\tresultList.add((T)entity);\n");

        stringMethod.append("\t}\n");

        stringMethod.append("\treturn resultList;\n");

        stringMethod.append("}");

        System.out.println(stringMethod);
    }

    public static void main(String[] args) throws ClassNotFoundException, NoSuchFieldException, InstantiationException, IllegalAccessException {
        //MySQLGenerator generator = new MySQLGenerator();
        //generator.generateConversionResultSetMethod(new Film());
        //generator.generateConversionResultSetMethod(new Actor());
    }
}
