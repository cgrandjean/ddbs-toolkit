package uk.co.cyril_grandjean.ddbstoolkit.web;

import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: Cyril
 * Date: 11/03/13
 * Time: 18:35
 * To change this template use File | Settings | File Templates.
 */
public class WebHandler extends AbstractHandler {

    private final String ACTION_PARAMETER = "action";



    @Override
    public void handle(String target, Request baseRequest, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        response.setContentType("application/json;charset=utf-8");
        response.setStatus(HttpServletResponse.SC_OK);
        baseRequest.setHandled(true);

        //Read parameters to create an action
        response.getWriter().println("<h1>Parameter Test = "+request.getParameter(ACTION_PARAMETER)+" </h1>");

        //Execute the action

        //return the results
    }
}
