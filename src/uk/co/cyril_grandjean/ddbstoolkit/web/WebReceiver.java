package uk.co.cyril_grandjean.ddbstoolkit.web;

import org.eclipse.jetty.server.Server;
import uk.co.cyril_grandjean.ddbstoolkit.core.DistributableEntityManager;
import uk.co.cyril_grandjean.ddbstoolkit.core.DistributableReceiverInterface;
import uk.co.cyril_grandjean.ddbstoolkit.core.Peer;

/**
 * Created with IntelliJ IDEA.
 * User: Cyril
 * Date: 23/02/13
 * Time: 17:42
 * To change this template use File | Settings | File Templates.
 */
public class WebReceiver implements DistributableReceiverInterface {

    /**
     * Web server
     */
    private Server myServer;

    /**
     * TableManager involved
     */
    DistributableEntityManager entityManager;

    /**
     * Port of the server
     */
    private int port = 8080;

    @Override
    public void start() throws Exception {

        myServer = new Server(port);
        myServer.setHandler(new WebHandler());
        myServer.start();
        myServer.join();
    }

    @Override
    public void stop() throws Exception {

        myServer.stop();
    }

    @Override
    public void setEntityManager(DistributableEntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public Peer getMyPeer() {
        return null;  //TODO
    }

    public static void main(String[] args) throws Exception
    {
        WebReceiver receiver = new WebReceiver();
        try
        {
            receiver.start();
        }
        finally {
            receiver.stop();
        }
    }
}
