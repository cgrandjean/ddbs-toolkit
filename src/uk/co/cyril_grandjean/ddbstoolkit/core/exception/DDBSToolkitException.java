package uk.co.cyril_grandjean.ddbstoolkit.core.exception;

/**
 * DDBSToolkit generic exception
 * @author Cyril Grandjean
 * @version 1.0: Class creation
 */
public class DDBSToolkitException extends Exception {

	public DDBSToolkitException(String message, Throwable throwable)
	{
		super(message, throwable);
	}
}
