package uk.co.cyril_grandjean.ddbstoolkit.core;

/**
 * Interface to generate a entity who will not be managed using reflection mechanism
 * Created with IntelliJ IDEA.
 * User: cgrandjean
 * Date: 26/09/13
 * Time: 10:03
 */
public interface GeneratableNonReflectedEntity {

    /**
     * Generate methods of the entity displayed in console output
     * @param entity
     */
    public void generateConversionResultSetMethod(IEntity entity) throws ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchFieldException;
}
